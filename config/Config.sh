#!/bin/sh

#MYSQL配置信息
export DB_BIN_PATH="/usr/local/Cellar/mysql@5.7/5.7.26/bin/"
export DB_HOST="10.4.250.10"
export DB_USER="opay"
export DB_PWD=""
export DB_SOCK=""
export DB_CHARACTER="utf8"
export DB_NAME="opay"

export BIN_MYSQL="${DB_BIN_PATH}mysql -h${DB_HOST} --default-character-set=${DB_CHARACTER} -u${DB_USER} -p${DB_PWD} "
export BIN_MYSQL_IMPORT="${DB_BIN_PATH}mysqlimport -h${DB_HOST} --default-character-set=${DB_CHARACTER} -u${DB_USER} -p${DB_PWD}"
export BIN_MYSQL_DUMP="${DB_BIN_PATH}mysqldump -h${DB_HOST} --default-character-set=${DB_CHARACTER} -u${DB_USER} -p${DB_PWD}"

#MongoDB配置信息
export MONGODB_BIN_PATH="/usr/local/Cellar/mongodb/4.0.3_1/bin/"
export MONGODB_HOST="127.0.0.1"
export MONGODB_PORT="27017"
export MONGODB_USER="mongo-admin-username"
export MONGODB_PWD="mongo-admin-secret"
export MONGODB_NAME="opay"
export MONGODB_PEM_KEYFILE="/Users/maohangjun/go/src/gitlab.services.ams.osa/opera-mobile/opera-pay/_deployment/mongodb/mongodb.pem"


export BIN_MONGODB_EXPORT="${MONGODB_BIN_PATH}mongoexport -h ${MONGODB_HOST} --port ${MONGODB_PORT} -u ${MONGODB_USER}  -p ${MONGODB_PWD}  --authenticationDatabase=admin --ssl --sslPEMKeyFile  ${MONGODB_PEM_KEYFILE}  --sslAllowInvalidCertificates --sslAllowInvalidHostnames"

