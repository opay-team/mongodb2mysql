#!/bin/sh

#string boolean转int
string_bool2int(){
    if [ "$1" = "true" ];then
        echo "1"
    else
        echo "0"
    fi
}

#string转int
string2int(){
    if [ "$1" = "null" ];then
        echo "0"
    else
        echo "$1"
    fi
}

#string null转datetime
string_null2datetime(){
    if [ "$1" = "null" ] || [ "$1" = "" ];then
        echo "0000-00-00 00:00:00"
    else
        echo "$1"
    fi
}

#string null转空
string_null2empty(){
    if [ "$1" = "null" ];then
        echo ""
    else
        echo "$1"
    fi
}

#string_bool2int true
#string_null2datetime null
#string_null2empty null
#string2int 1