# 数据迁移 MongoDB2Mysql

## 一、服务器环境安装
### 1、安装Mongo
```
$ vim /etc/yum.repos.d/mongodb-org-3.4.repo
$ cat /etc/yum.repos.d/mongodb-org-3.4.repo
[mongodb-org-3.4] 
name=MongoDB Repository  
baseurl=https://repo.mongodb.org/yum/redhat/$releasever/mongodb-org/3.4/x86_64/  
gpgcheck=0
enabled=1
gpgkey=https://www.mongodb.org/static/pgp/server-3.4.asc

$ yum install mongodb-org
```
### 2、安装Mysql
```
$ wget -c http://dev.mysql.com/get/mysql57-community-release-el7-10.noarch.rpm
$ yum install mysql57-community-release-el7-10.noarch.rpm
$ yum install mysql-community-server
```

### 3、安装PHP
```
$ rpm -Uvh https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
$ rpm -Uvh https://mirror.webtatic.com/yum/el7/webtatic-release.rpm
$ yum install php71w php71w-fpm php71w-cli php71w-common php71w-devel php71w-gd php71w-pdo php71w-mysql php71w-mbstring php71w-bcmath

$ yum install php71w-mcrypt
```

### 4、安装jq
```
CentOS
$ yum install epel-release
$ yum install jq
```

### 5、安装其它
```
$ yum install bc
```

### 6、优化性能
```
$ vim /etc/php.ini
memory_limit = -1

$ vim /etc/security/limits.conf
* soft nproc unlimited
* hard nproc unlimited
* soft nofile 655350
* hard nofile 655350

$ vim /etc/sysctl.conf
fs.file-max = 6553560

$ mysql
set global max_allowed_packet=1073741824;
set global max_connections=16000;
set global max_connect_errors=10000;
set global net_buffer_length=300000;
set global interactive_timeout=28800;
set global wait_timeout=28800;
set global tmp_table_size=268435456;
set global max_heap_table_size=268435456;
set global innodb_flush_log_at_trx_commit=0;
flush hosts;

如果有MySQL Error "Incorrect integer value" for column 'name' at row 1″错误
SET GLOBAL sql_mode = '';
或修改my.cnf
sql_mode=NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION
```

## 二、数据库连接配置文件
```
$ cat config/Config.sh

#MYSQL配置信息
export DB_BIN_PATH="/usr/local/Cellar/mysql@5.7/5.7.26/bin/"
export DB_HOST="127.0.0.1"
export DB_USER="opay"
export DB_PWD=""
export DB_SOCK=""
export DB_CHARACTER="utf8"
export DB_NAME="opay"

#MongoDB配置信息
export MONGODB_BIN_PATH="/usr/local/Cellar/mongodb/4.0.3_1/bin/"
export MONGODB_HOST="127.0.0.1"
export MONGODB_PORT="27017"
export MONGODB_USER="mongo-admin-username"
export MONGODB_PWD="mongo-admin-secret"
export MONGODB_NAME="opay"
export MONGODB_PEM_KEYFILE="mongodb.pem"
```


## 三、整表清空与导入
```
sh ./fees.sh > tmp/fees.log
sh ./blacklisted_instrument.sh > tmp/blacklisted_instrument.log
sh ./financial_institutions.sh > tmp/financial_institutions.log
sh ./configuration.sh > tmp/configuration.log
sh ./overlord_users.sh > tmp/overlord_users.log
sh ./terminals.sh > tmp/terminals.log
sh ./terminal_owners.sh > tmp/terminal_owners.log
sh ./payment_terminal_providers.sh > tmp/payment_terminal_providers.log
sh ./merchant.sh > tmp/merchant.log
sh ./businesses.sh > tmp/businesses.log
sh ./business_users.sh > tmp/business_users.log

#230万行，整体20分钟
#sh ./user.sh > tmp/user.log &

#800万行，整体30分钟
#sh ./payment_instruments.sh > tmp/payment_instruments.log &

#以下是余额相关的 V3
#2000万行，提前导，整体40分钟
#sh ./coins_transactions.sh > tmp/coins_transactions.log &

#以下是余额相关的 V2
#3600多万行，提前导，整体1.5个小时
#sh ./chains.sh > tmp/chains.log &
#7000多万行，提前导，导出来50G，1.5个小时导出，整体5个小时
#sh ./chain_boxes.sh > tmp/chain_boxes.log &
```

## 四、增量导入，从某天0点开始到当日结束
```
$ sh payment_instruments_byday.sh 2019 08 30

$ sh coins_transactions_byday.sh 2019 08 30

$ sh chains_byday.sh 2019 08 30

$ sh chain_boxes_byday.sh 2019 08 30
```

## 五、定时任务
```
10 1 * * * sh /work/crontab.sh > /dev/null 2>&1
```

## 六、辅助功能

### 1、查询V2/V3余额（用户、商户）
```
同时查询MongoDb与Mysql用法
$ sh checkCoins.sh [用户|商户] [id|手机号] 对应的值
$ sh checkCoins.sh [user|businesses] [id|phonenumber] value

用户
$ sh checkCoins.sh user phonenumber +2348051584655
商户
$ sh checkCoins.sh businesses phonenumber +2348067779014

FromMysql
用户
$ sh checkCoinsFromMysql.sh user phonenumber +2348051584655
商户
$ sh checkCoinsFromMysql.sh businesses phonenumber +2348067779014

FromMongDb
用户
$ sh checkCoinsFromMongoDb.sh user phonenumber +2348051584655
商户
$ sh checkCoinsFromMongoDb.sh businesses phonenumber +2348067779014
```

### 2、解密payment_instruments加密串
```
$ ./decrypt_production -text="加密串"
V2: {"box_name":"5d0d1224701eef000195a5e7","account":"main"}
V3: {"box_name":"5d36a2f5997ca40001d8dfa7","chain_id":"5d36a2f5997ca40001d8dfab","account":"main","currency":"NGN"}
```

### 3、检查导出文件行数与数据库导入行数
```
$ sh checkLineNum.sh
```

### 4、查询线上系统余额接口,内部使用，注意请求限制并发
```
$ sh checkCoinsByGraphql.sh user 5d7108e3cac6da0001b15010
$ sh checkCoinsByGraphql.sh businesses 123456123456123456999910
$ sh checkCoinsByGraphql.sh owealth 5d7108e3cac6da0001b15010
```

### 5、随机抽检余额
```
$ sh checkCoinsRand.sh user
$ sh checkCoinsRand.sh businesses
$ sh checkCoinsRand.sh owealth
```

