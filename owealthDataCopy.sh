#!/bin/sh

export DATA_SHELL_PATH=$(cd "$(dirname "$0")"; pwd)
source ${DATA_SHELL_PATH}/config/Config.sh

echo ">>>导出owealth表"
${BIN_MYSQL_DUMP} ${DB_NAME} transactions_owealth_balance > ${DATA_SHELL_PATH}/data/owealth_balance.sql 2>/dev/null
${BIN_MYSQL_DUMP} ${DB_NAME} transactions_owealth > ${DATA_SHELL_PATH}/data/transactions_owealth.sql 2>/dev/null


echo ">>>导入owealth表到测试环境"
TEST_IP="54.229.88.73"
TEST_USER="root"
TEST_PASS=""
TEST_DB="opay_owealth"
TEST_TABLE="share_acct_test"


#导入到测试服务器
mysql -h"${TEST_IP}" --default-character-set=utf8 -u${TEST_USER} -p${TEST_PASS} ${TEST_DB} < ${DATA_SHELL_PATH}/data/owealth_balance.sql
mysql -h"${TEST_IP}" --default-character-set=utf8 -u${TEST_USER} -p${TEST_PASS} ${TEST_DB} < ${DATA_SHELL_PATH}/data/transactions_owealth.sql

echo ">>>清空:${TEST_DB}.${TEST_TABLE}"
mysql -h"${TEST_IP}" --default-character-set=utf8 -u${TEST_USER} -p${TEST_PASS} -e"TRUNCATE TABLE ${TEST_DB}.${TEST_TABLE}"

echo ">>>导入数据:${TEST_DB}.${TEST_TABLE}"
mysql -h"${TEST_IP}" --default-character-set=utf8 -u${TEST_USER} -p${TEST_PASS} -e"INSERT INTO ${TEST_DB}.${TEST_TABLE} (share_acct_id,contract_id,merchant_id,product_code,user_id,asset_type,balance,freeze_amount,revenue_amount,last_revenue,currency,acct_status,create_time,update_time,create_operator,memo) select phonenumber,concat('M100000000_',phonenumber),'M100000000','S10000',phonenumber,'1001',balance_floatvalue * 100,0,0,0,'NGN','N',now(),now(),'manual','导入用户' from ${TEST_DB}.transactions_owealth_balance"

echo ">>>处理历史订单记录:"
#//申购//赎回
mysql -h"${TEST_IP}" --default-character-set=utf8 -u${TEST_USER} -p${TEST_PASS} -e"DELETE FROM ${TEST_DB}.share_trans_log where create_operator='import'"
mysql -h"${TEST_IP}" --default-character-set=utf8 -u${TEST_USER} -p${TEST_PASS} -e"INSERT INTO ${TEST_DB}.share_trans_log (acct_log_id,share_acct_id,merchant_id,product_code,user_id,order_id,order_type,trans_amount,create_time,update_time,create_operator,memo) SELECT id,recipient_user_phonenumber,'M100000000','S10000',recipient_user_phonenumber,id,'1001',value*100,timestamp,timestamp,'import','申购' from ${TEST_DB}.transactions_owealth where service_type='invest'"
mysql -h"${TEST_IP}" --default-character-set=utf8 -u${TEST_USER} -p${TEST_PASS} -e"INSERT INTO ${TEST_DB}.share_trans_log (acct_log_id,share_acct_id,merchant_id,product_code,user_id,order_id,order_type,trans_amount,create_time,update_time,create_operator,memo) SELECT id,recipient_user_phonenumber,'M100000000','S10000',recipient_user_phonenumber,id,'1002',value*100,timestamp,timestamp,'import','赎回' from ${TEST_DB}.transactions_owealth where service_type='coins-transfer'"
#//投资收益
mysql -h"${TEST_IP}" --default-character-set=utf8 -u${TEST_USER} -p${TEST_PASS} -e"DELETE FROM ${TEST_DB}.share_revenue_log where memo='import'"
mysql -h"${TEST_IP}" --default-character-set=utf8 -u${TEST_USER} -p${TEST_PASS} -e"INSERT INTO ${TEST_DB}.share_revenue_log (revenue_id,batch_no,merchant_id,product_code,user_id,share_acct_id,revenue_amount,revenue_date,create_time,update_time,memo) SELECT id,CONCAT(DATE_FORMAT(timestamp,'%Y-%m-%d'),'_M100000000_M'),'M100000000','S10000',recipient_user_phonenumber,recipient_user_phonenumber,value*100,timestamp,timestamp,timestamp,'import' from ${TEST_DB}.transactions_owealth where service_type='invest-interest'"


echo ">>>完成"




echo ">>>导入owealth表到正式环境"
PRODUCTION_IP="opay-owealth-prod.c7vkdm90b2v9.eu-west-1.rds.amazonaws.com"
PRODUCTION_USER="root"
PRODUCTION_PASS=""
PRODUCTION_DB="opay_owealth"
PRODUCTION_TABLE="share_acct_test"


#导入到正式服务器
mysql -h"${PRODUCTION_IP}" --default-character-set=utf8 -u${PRODUCTION_USER} -p${PRODUCTION_PASS} ${PRODUCTION_DB} < ${DATA_SHELL_PATH}/data/owealth_balance.sql
mysql -h"${PRODUCTION_IP}" --default-character-set=utf8 -u${PRODUCTION_USER} -p${PRODUCTION_PASS} ${PRODUCTION_DB} < ${DATA_SHELL_PATH}/data/transactions_owealth.sql

echo ">>>清空:${PRODUCTION_DB}.${PRODUCTION_TABLE}"
mysql -h"${PRODUCTION_IP}" --default-character-set=utf8 -u${PRODUCTION_USER} -p${PRODUCTION_PASS} -e"TRUNCATE TABLE ${PRODUCTION_DB}.${PRODUCTION_TABLE}"

echo ">>>导入数据:${PRODUCTION_DB}.${PRODUCTION_TABLE}"
mysql -h"${PRODUCTION_IP}" --default-character-set=utf8 -u${PRODUCTION_USER} -p${PRODUCTION_PASS} -e"INSERT INTO ${PRODUCTION_DB}.${PRODUCTION_TABLE} (share_acct_id,contract_id,merchant_id,product_code,user_id,asset_type,balance,freeze_amount,revenue_amount,last_revenue,currency,acct_status,create_time,update_time,create_operator,memo) select phonenumber,concat('M100000000_',phonenumber),'M100000000','S10000',phonenumber,'1001',balance_floatvalue * 100,0,0,0,'NGN','N',now(),now(),'manual','导入用户' from ${PRODUCTION_DB}.transactions_owealth_balance"

echo ">>>处理历史订单记录:"
#//申购//赎回
mysql -h"${PRODUCTION_IP}" --default-character-set=utf8 -u${PRODUCTION_USER} -p${PRODUCTION_PASS} -e"DELETE FROM ${PRODUCTION_DB}.share_trans_log where create_operator='import'"
mysql -h"${PRODUCTION_IP}" --default-character-set=utf8 -u${PRODUCTION_USER} -p${PRODUCTION_PASS} -e"INSERT INTO ${PRODUCTION_DB}.share_trans_log (acct_log_id,share_acct_id,merchant_id,product_code,user_id,order_id,order_type,trans_amount,create_time,update_time,create_operator,memo) SELECT id,recipient_user_phonenumber,'M100000000','S10000',recipient_user_phonenumber,id,'1001',value*100,timestamp,timestamp,'import','申购' from ${PRODUCTION_DB}.transactions_owealth where service_type='invest'"
mysql -h"${PRODUCTION_IP}" --default-character-set=utf8 -u${PRODUCTION_USER} -p${PRODUCTION_PASS} -e"INSERT INTO ${PRODUCTION_DB}.share_trans_log (acct_log_id,share_acct_id,merchant_id,product_code,user_id,order_id,order_type,trans_amount,create_time,update_time,create_operator,memo) SELECT id,recipient_user_phonenumber,'M100000000','S10000',recipient_user_phonenumber,id,'1002',value*100,timestamp,timestamp,'import','赎回' from ${PRODUCTION_DB}.transactions_owealth where service_type='coins-transfer'"
#//投资收益
mysql -h"${PRODUCTION_IP}" --default-character-set=utf8 -u${PRODUCTION_USER} -p${PRODUCTION_PASS} -e"DELETE FROM ${PRODUCTION_DB}.share_revenue_log where memo='import'"
mysql -h"${PRODUCTION_IP}" --default-character-set=utf8 -u${PRODUCTION_USER} -p${PRODUCTION_PASS} -e"INSERT INTO ${PRODUCTION_DB}.share_revenue_log (revenue_id,batch_no,merchant_id,product_code,user_id,share_acct_id,revenue_amount,revenue_date,create_time,update_time,memo) SELECT id,CONCAT(DATE_FORMAT(timestamp,'%Y-%m-%d'),'_M100000000_M'),'M100000000','S10000',recipient_user_phonenumber,recipient_user_phonenumber,value*100,timestamp,timestamp,timestamp,'import' from ${PRODUCTION_DB}.transactions_owealth where service_type='invest-interest'"

echo ">>>完成"