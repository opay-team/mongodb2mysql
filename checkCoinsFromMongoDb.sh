#!/bin/sh
#检查用户余额正确性支持V2/V3查询
#数据来源MongoDB

export DATA_SHELL_PATH=$(cd "$(dirname "$0")"; pwd)
source ${DATA_SHELL_PATH}/config/Config.sh

usage() {
    echo "Usage: sh $0 type field phonenumber"
    echo "Example: sh $0 [user|businesses] [id|phonenumber] +2348051584655"
    echo "sh $0 user phonenumber +2348051584655"
    echo "sh $0 businesses phonenumber +2348067779014"
    exit 1
}

if [ $# != 3 ]; then
    usage
fi

FIELD_VALUE=$3
FIELD=$2
TYPE=$1

if [ "${FIELD}" == "id" ];then
    if [ "${TYPE}" == "businesses" ];then
        user_id_string=`${BIN_MONGODB_EXPORT} -d ${MONGODB_NAME} -c businesses -f '_id' --quiet --query "{'_id' : ObjectId('${FIELD_VALUE}')}"`
    else
        user_id_string=`${BIN_MONGODB_EXPORT} -d ${MONGODB_NAME} -c users -f '_id' --quiet --query "{'_id' : ObjectId('${FIELD_VALUE}')}"`
    fi
else
    if [ "${TYPE}" == "businesses" ];then
        user_id_string=`${BIN_MONGODB_EXPORT} -d ${MONGODB_NAME} -c businesses -f '_id' --quiet --query "{'phonenumber' : '${FIELD_VALUE}'}"`
    else
        user_id_string=`${BIN_MONGODB_EXPORT} -d ${MONGODB_NAME} -c users -f '_id' --quiet --query "{'phonenumber' : '${FIELD_VALUE}'}"`
    fi
fi

user_id_string=`echo "${user_id_string}"|sed 's/\"$oid\"/\"oid\"/g'`
user_id=`echo "${user_id_string}" | jq -r -c '._id.oid'`

if [ "${user_id}" = "" ];then
    echo "user/businesses not found."
    exit
fi

main_data_cipher_text_string=`${BIN_MONGODB_EXPORT} -d ${MONGODB_NAME} -c payment_instruments -f 'data' --quiet --query "{'user_id' : ObjectId('${user_id}'),'scheme':'Wallet'}"`
main_data_cipher_text_string=`echo "${main_data_cipher_text_string}"|sed 's/\"$oid\"/\"oid\"/g'`
main_data_cipher_text_string=`echo "${main_data_cipher_text_string}"|sed 's/\"$binary\"/\"binary\"/g'`
main_data_cipher_text=`echo "${main_data_cipher_text_string}" | jq -r -c '.data.cipher_text.binary'`


#解密
json_string=`${DATA_SHELL_PATH}/decrypt_production -text="${main_data_cipher_text}"`

box_name=`echo "${json_string}" | jq -r -c '.box_name'`
chain_id=`echo "${json_string}" | jq -r -c '.chain_id'`

if [ "${chain_id}" != "" ] && [ "${chain_id}" != "null" ] ;then
    balance_after=`${BIN_MONGODB_EXPORT} -d ${MONGODB_NAME} -c coins_transactions -f 'balance_after' --quiet --query "{'chain_id' : ObjectId('${chain_id}')}" --sort="{'counter': -1}" --limit=1`
    amount=`echo "${balance_after}" | jq -r -c '.balance_after.available_balance.value'`
    available_balance=`echo "${balance_after}" | jq -c '.balance_after.available_balance'`
    echo -e "MongoDBV3:\t${user_id}\t${amount}\t${chain_id}\t${available_balance}"
else
    if [ "${box_name}" != "" ] && [ "${box_name}" != "null" ] ;then
        #box_id_string=`${BIN_MONGODB_EXPORT} -d ${MONGODB_NAME} -c chains -f 'box_ids' --quiet --query "{'_id' : ObjectId('${box_name}')}"`
        #box_id_string=`echo "${box_id_string}"|sed 's/\"$oid\"/\"oid\"/g'`
        #box_id=`echo "${box_id_string}"|jq -c ".box_ids[-1]"|jq -r -c ".oid"`

        #transactions_string=`${BIN_MONGODB_EXPORT} -d ${MONGODB_NAME} -c chain_boxes -f 'transactions' --quiet --query "{'_id' : ObjectId('${box_id}')}"`
        #transactions_string=`echo "${transactions_string}"|sed 's/\"$oid\"/\"oid\"/g'`
        #transactions_string=`echo "${transactions_string}"|jq -c ".transactions[-1]"`
        #transaction_balance_after=`echo "${transactions_string}"|jq -r -c ".transaction.balance_after"`

        #amount=`echo "${transaction_balance_after}" | jq -r -c '.NGN.main.value'`
        #transaction_balance_after=`echo "${transaction_balance_after}" | jq -c '.NGN'`
        #echo -e "MongoDBV2:\t${user_id}\t${amount}\t${box_name}\t${box_id}\t${transaction_balance_after}"
        echo -e "MongoDBV2:\t${user_id}\t${amount}\t${box_name}\t查询暂不可用"
    fi
fi