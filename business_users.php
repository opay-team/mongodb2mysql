#!/usr/bin/php
<?php
define('DS', DIRECTORY_SEPARATOR);
define('ROOT_PATH', realpath(dirname(__FILE__).DS).DS);
include_once (ROOT_PATH."global.php");

$file = @file($in_filename);
foreach($file as $line)
{
    //echo $line;
    $line_json = json_decode($line,true);
    if(!is_array($line_json)){
        @error_log($line."\n",3,"/tmp/mongodb_mysql_error.log");
        continue;
    }
    //var_dump($line_json);
    $id=$line_json['_id']['$oid'];
    $linked_businesses=json_encode($line_json['linked_businesses']);
    $email=addslashes($line_json['email']);
    $firstname=addslashes($line_json['firstname']);
    $lastname=addslashes($line_json['lastname']);
    $password_hash=$line_json['password']['hash']['$binary'];
    $password_salt=$line_json['password']['salt']['$binary'];
    $bvn=json_encode($line_json['bvn']);
    $reset_token=$line_json['reset_token'];
    $last_visit=str_replace("Z","",str_replace("T"," ",$line_json['last_visit']['$date']));
    $created_at=str_replace("Z","",str_replace("T"," ",$line_json['created_at']['$date']));
    $access_role=$line_json['access_role'];
    $phone_number=$line_json['phone_number'];

    $sql= "INSERT INTO business_users (`id`, `linked_businesses`, `email`, `firstname`, `lastname`, `password_hash`, `password_salt`, `bvn`, `reset_token`, `last_visit`, `created_at`, `access_role`, `phone_number` ) VALUES ('${id}','${linked_businesses}','${email}','${firstname}','${lastname}','${password_hash}','${password_salt}','${bvn}','${reset_token}','${last_visit}','${created_at}','${access_role}','${phone_number}');";
    //echo $sql."\n";
    @error_log($sql."\n",3,$out_filename);
}
?>
