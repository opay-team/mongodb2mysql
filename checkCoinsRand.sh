#!/bin/sh
#查询比较，随机取20条，进行比对

export DATA_SHELL_PATH=$(cd "$(dirname "$0")"; pwd)
source ${DATA_SHELL_PATH}/config/Config.sh

usage() {
    echo "Usage: sh $0 type"
    echo "Example: sh $0 [user|businesses|owealth]"
    echo "sh $0 user"
    echo "sh $0 businesses"
    echo "sh $0 owealth"
    exit 1
}

if [ $# != 1 ]; then
    usage
fi

TYPE=$1
ID=$2

if [ "${TYPE}" == "user" ];then
    users=`${BIN_MYSQL} --skip-column-names -e "SELECT id from ${DB_NAME}.user ORDER BY RAND() limit 20" 2>/dev/null|while read a b;do echo "$a:$b";done`

    for u in ${users};do
        uid=`echo $u |cut -d: -f 1`

        result=`sh ${DATA_SHELL_PATH}/checkCoinsFromMysql.sh user id ${uid}`
        mysql_balance=`echo "${result}"|awk -F'\t' '{print $3}'`
        version=`echo "${result}"|awk -F'\t' '{print $1}'`

        now_balance=`sh ${DATA_SHELL_PATH}/checkCoinsByGraphql.sh user ${uid}`
        now_balance=`awk -v n=${now_balance} 'BEGIN{printf "%0.2f",n}'`

        difference=$(echo "${mysql_balance}-${now_balance}"|bc)

        echo -e "User:\t${version}\t${uid}\t${mysql_balance}\t${now_balance}\t\t\t\t\t\t\t${difference}"

        sleep 1
    done
fi


if [ "${TYPE}" == "businesses" ];then
    business_users=`${BIN_MYSQL} --skip-column-names -e "SELECT id from ${DB_NAME}.businesses ORDER BY RAND() limit 20" 2>/dev/null|while read a b;do echo "$a:$b";done`

    for u in ${business_users};do
        uid=`echo $u |cut -d: -f 1`

        result=`sh ${DATA_SHELL_PATH}/checkCoinsFromMysql.sh businesses id ${uid}`
        mysql_balance=`echo "${result}"|awk -F'\t' '{print $3}'`
        version=`echo "${result}"|awk -F'\t' '{print $1}'`


        now_balance=`sh ${DATA_SHELL_PATH}/checkCoinsByGraphql.sh businesses ${uid}`
        now_balance=`awk -v n=${now_balance} 'BEGIN{printf "%0.2f",n}'`

        difference=$(echo "${mysql_balance}-${now_balance}"|bc)

        echo -e "Business:\t${version}\t${uid}\t${mysql_balance}\t${now_balance}\t\t\t\t\t\t\t${difference}"

        sleep 1
    done
fi

if [ "${TYPE}" == "owealth" ];then
    owealth_users=`${BIN_MYSQL} --skip-column-names -e "SELECT user_id,balance_floatvalue FROM  ${DB_NAME}.transactions_owealth_balance ORDER BY RAND() limit 20" 2>/dev/null|while read a b;do echo "$a:$b";done`

    for u in ${owealth_users};do
        uid=`echo $u |cut -d: -f 1`
        mysql_balance=`echo $u |cut -d: -f 2`

        now_balance=`sh ${DATA_SHELL_PATH}/checkCoinsByGraphql.sh owealth ${uid}`
        now_balance=`awk -v n=${now_balance} 'BEGIN{printf "%0.2f",n}'`

        difference=$(echo "${mysql_balance}-${now_balance}"|bc)

        echo -e "Owealth:\t${uid}\t${mysql_balance}\t${now_balance}\t\t\t\t\t\t\t${difference}"

        sleep 1
    done

fi