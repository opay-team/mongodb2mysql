#!/bin/sh
#循环日志执行导数脚本，主要针对按时间导数的表

exit

usage() {
    echo "Usage: sh $0 脚本名称 开始年月日 结束年月日"
    echo "Example: sh $0 chains_byday.sh 20190827 20190902"
    exit 1
}

if [ $# != 3 ]; then
    usage
    exit 1
fi

EXPORT_SHELL_SCRIPT=$1
EXPORT_START_DATE=$2
EXPORT_END_DATE=$3

if [ ! -f "${EXPORT_SHELL_SCRIPT}"  ];then
    echo "脚本:${EXPORT_SHELL_SCRIPT}不存在"
    exit 1
fi

i=$EXPORT_START_DATE
while [[ $i < `date -d "+1 day $EXPORT_END_DATE" +%Y%m%d` ]];do
    #echo $i
    y=`date +%Y -d "$i"`
    m=`date +%m -d "$i"`
    d=`date +%d -d "$i"`

    echo "开始处理：${y} ${m} ${d}"
	sh ${EXPORT_SHELL_SCRIPT} ${y} ${m} ${d}

    i=`date -d "+1 day $i" +%Y%m%d`
done