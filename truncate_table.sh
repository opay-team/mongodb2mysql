#!/bin/sh
#清空数据表

export DATA_SHELL_PATH=$(cd "$(dirname "$0")"; pwd)
source ${DATA_SHELL_PATH}/config/Config.sh
source ${DATA_SHELL_PATH}/config/Util.sh

usage() {
    echo "Usage: sh $0 相关表"
    echo "Example: sh $0 businesses"
    exit 1
}

if [ $# != 1 ]; then
    usage
    exit 1
fi

read -r -p "要清空数据表吗？ [y/n] " input

istruncate="false"
case $input in
          [yY])
           echo "清除数据表……"
           istruncate="true"
           ;;
          [nN])
           echo "No,程序已退出..."
           ;;
       *)
            echo "程序已退出..."
            exit 1
            ;;
esac

if [ "${istruncate}" = "true" ];then
    if [ "$1" = "user" ];then
        #清空用户相关表
        ${BIN_MYSQL} -e "TRUNCATE TABLE ${DB_NAME}.user"
        ${BIN_MYSQL} -e "TRUNCATE TABLE ${DB_NAME}.user_detail"
        ${BIN_MYSQL} -e "TRUNCATE TABLE ${DB_NAME}.user_bvn"
        ${BIN_MYSQL} -e "TRUNCATE TABLE ${DB_NAME}.document"
        ${BIN_MYSQL} -e "TRUNCATE TABLE ${DB_NAME}.user_identification"
        ${BIN_MYSQL} -e "TRUNCATE TABLE ${DB_NAME}.user_telesale"
        ${BIN_MYSQL} -e "TRUNCATE TABLE ${DB_NAME}.user_upgrade"
        ${BIN_MYSQL} -e "TRUNCATE TABLE ${DB_NAME}.user_aggregators"
        ${BIN_MYSQL} -e "TRUNCATE TABLE ${DB_NAME}.user_linked_users"
        ${BIN_MYSQL} -e "TRUNCATE TABLE ${DB_NAME}.user_reseller_invite"
        ${BIN_MYSQL} -e "TRUNCATE TABLE ${DB_NAME}.user_service_config"
    fi

    if [ "$1" = "businesses" ];then
        #清空商户相关表
        ${BIN_MYSQL} -e "TRUNCATE TABLE ${DB_NAME}.businesses"
        ${BIN_MYSQL} -e "TRUNCATE TABLE ${DB_NAME}.businesses_merchant_email_settings"
        ${BIN_MYSQL} -e "TRUNCATE TABLE ${DB_NAME}.businesses_merchant_key"
        ${BIN_MYSQL} -e "TRUNCATE TABLE ${DB_NAME}.businesses_merchant_reseller_invites"
        ${BIN_MYSQL} -e "TRUNCATE TABLE ${DB_NAME}.businesses_merchant_user_invites"
        ${BIN_MYSQL} -e "TRUNCATE TABLE ${DB_NAME}.business_aggregators"
    fi

    if [ "$1" = "business_users" ];then
        #清空表数据
        ${BIN_MYSQL} -e "TRUNCATE TABLE ${DB_NAME}.business_users"
    fi

    if [ "$1" = "payment_instruments" ];then
        #清空余额关联表数据
        ${BIN_MYSQL} -e "TRUNCATE TABLE ${DB_NAME}.payment_instruments"
    fi

    if [ "$1" = "coins_transactions" ];then
        #清空余额表数据
        ${BIN_MYSQL} -e "TRUNCATE TABLE ${DB_NAME}.coins_transactions"
    fi
fi