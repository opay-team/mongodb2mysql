#!/bin/sh

export DATA_SHELL_PATH=$(cd "$(dirname "$0")"; pwd)
source ${DATA_SHELL_PATH}/config/Config.sh
source ${DATA_SHELL_PATH}/config/Util.sh

#临时文件配置
#导出表名
_TABLE_NAME="badactor_infractions"
#日志处理脚本名称
_PHP_SHELL_NAME="badactor_infractions.php"
#日志分割行数
_LOG_SPLIT_NUM="30000"
#MONGODB导出的JSON文件
_TMP_DATA_JSON=${DATA_SHELL_PATH}/data/${_TABLE_NAME}.json

date
echo ">>>开始处理数据:${_TABLE_NAME}"

#清空表数据
${BIN_MYSQL} -e "TRUNCATE TABLE ${DB_NAME}.${_TABLE_NAME}" 2>/dev/null

#从MongoDB中导出表数据Json格式
${BIN_MONGODB_EXPORT} -d ${MONGODB_NAME} -c ${_TABLE_NAME} -o ${_TMP_DATA_JSON}

echo ">>>导出MongoDB数据结束:"
date
echo ">>>MongoDB->Mysql:"

#批量处理日志存放目录
_TMP_DATA_SUBDIR=${DATA_SHELL_PATH}/data/${_TABLE_NAME}/

mkdir -p ${_TMP_DATA_SUBDIR}
mkdir -p ${_TMP_DATA_SUBDIR}/tmp/
mkdir -p ${_TMP_DATA_SUBDIR}/txt/

rm -rf ${_TMP_DATA_SUBDIR}/split_*
rm -rf ${_TMP_DATA_SUBDIR}/tmp/split_*
rm -rf ${_TMP_DATA_SUBDIR}/txt/split_*

#分割原始文件
split -l ${_LOG_SPLIT_NUM} ${_TMP_DATA_JSON} "${_TMP_DATA_SUBDIR}/split_${_TABLE_NAME}."

for f in `find ${_TMP_DATA_SUBDIR}/ -name "split_*"`;do
    f=${f##*/}

#多进程放后台并发执行
{

    sql_file="${_TMP_DATA_SUBDIR}/txt/${f}.txt"

    rm -rf ${sql_file}
    #将MongDB Json格式导成Mysql表格式
    php ${DATA_SHELL_PATH}/${_PHP_SHELL_NAME} "${_TMP_DATA_SUBDIR}/${f}" "${sql_file}"

    echo ">>>导入Mysql:${sql_file}"

    #导入数据
    if [ -f "${sql_file}" ];then
        ${BIN_MYSQL} -f ${DB_NAME} < ${sql_file} 2>>${DATA_SHELL_PATH}/tmp/${_TABLE_NAME}.error.log
    fi
} &

done

wait

import_nums=`${BIN_MYSQL} --skip-column-names -e "SELECT COUNT(*) FROM ${DB_NAME}.${_TABLE_NAME}" 2>/dev/null`
echo ">>>导入数据条数：${import_nums}"

echo ">>>处理完毕"
date