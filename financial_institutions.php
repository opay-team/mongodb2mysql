#!/usr/bin/php
<?php
define('DS', DIRECTORY_SEPARATOR);
define('ROOT_PATH', realpath(dirname(__FILE__).DS).DS);
include_once (ROOT_PATH."global.php");

$file = @file($in_filename);
foreach($file as $line)
{
    //echo $line;
    $line_json = json_decode($line,true);
    if(!is_array($line_json)){
        @error_log($line."\n",3,"/tmp/mongodb_mysql_error.log");
        continue;
    }
    //var_dump($line_json);
    $id=$line_json['_id']['$oid'];
    $alt_code=$line_json['alt_code'];
    $code=$line_json['code'];
    $display_name=addslashes($line_json['display_name']);
    $category_code=$line_json['category_code'];
    $category=addslashes($line_json['category']);
    $country=$line_json['country'];
    $update_batch_number=$line_json['update_batch_number'];

    $sql="INSERT INTO financial_institutions (`id`, `alt_code`, `code`, `display_name`, `category_code`, `category`, `country`, `update_batch_number` ) VALUES ('${id}','${alt_code}','${code}','${display_name}','${category_code}','${category}','${country}','${update_batch_number}');";
    //echo $sql."\n";
    @error_log($sql."\n",3,$out_filename);
}
?>
