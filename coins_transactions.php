#!/usr/bin/php
<?php
define('DS', DIRECTORY_SEPARATOR);
define('ROOT_PATH', realpath(dirname(__FILE__).DS).DS);
include_once (ROOT_PATH."global.php");

$file = @file($in_filename);
foreach($file as $line)
{
    //echo $line;
    $line_json = json_decode($line,true);
    if(!is_array($line_json)){
        @error_log($line."\n",3,"/tmp/coins_transactions_error.log");
        continue;
    }
    //var_dump($line_json);
    $id=$line_json['_id']['$oid'];
    $previous_id=$line_json['previous_id']['$oid'];
    $chain_id=$line_json['chain_id']['$oid'];
    $counter=$line_json['counter'];
    $version=$line_json['version'];
    $type=$line_json['type'];
    $timestamp=str_replace("Z","",str_replace("T"," ",$line_json['timestamp']['$date']));
    $reference=$line_json['reference'];
    $amount=json_encode($line_json['amount']);
    $balance_before=json_encode($line_json['balance_before']);
    $balance_after=json_encode($line_json['balance_after']);
    $signature="";//此值暂无用 $line_json['signature']['$binary'];
    $data=$line_json['data'];

    if(!empty($id)) {
        $sql_start = "INSERT";
        if($sql_type=="REPLACE"){
            $sql_start = "REPLACE";
        }
        $sql = "$sql_start INTO coins_transactions (`id`, `previous_id`, `chain_id`, `counter`, `version`, `type`, `timestamp`, `reference`, `amount`, `balance_before`, `balance_after`, `signature`, `data` ) VALUES ('${id}','${previous_id}','${chain_id}','${counter}','${version}','${type}','${timestamp}','${reference}','${amount}','${balance_before}','${balance_after}','${signature}','${data}');";
        //echo $sql."\n";
        @error_log($sql . "\n", 3, $out_filename);
    }
}
?>
