#!/usr/bin/php
<?php
define('DS', DIRECTORY_SEPARATOR);
define('ROOT_PATH', realpath(dirname(__FILE__).DS).DS);
include_once (ROOT_PATH."global.php");

$file = @file($in_filename);
foreach($file as $line)
{
    //echo $line;
    $line_json = json_decode($line,true);
    if(!is_array($line_json)){
        @error_log($line."\n",3,"/tmp/mongodb_mysql_error.log");
        continue;
    }
    //var_dump($line_json);
    $id=$line_json['_id']['$oid'];

    $merchant_id=$line_json['merchant_id']['$oid'];
    $linked_users=json_encode($line_json['linked_users']);
    $name=addslashes($line_json['name']);
    $category=$line_json['category'];
    $key_pairs="";
    $webhook_url=$line_json['webhook_url'];
    $allowed_to_go_live=$line_json['allowed_to_go_live']?"1":"0";
    $instant_settlement=$line_json['instant_settlement']?"1":"0";
    $countries=json_encode($line_json['countries']);
    $skip_commit_stage=$line_json['skip_commit_stage']?"1":"0";
    $pos_transaction_limit=json_encode($line_json['pos_transaction_limit']);
    $user_invites="";
    $reseller_invites="";
    $remittance_config=json_encode($line_json['remittance_config']);
    $email_settings="";
    $limits=json_encode($line_json['limits']);
    $disable_settlements=$line_json['disable_settlements']?"1":"0";
    $timestamp=str_replace("Z","",str_replace("T"," ",$line_json['timestamp']['$date']));
    $settlement_period=$line_json['settlement_period'];
    $phonenumber=$line_json['phonenumber'];
    $sandbox_id=$line_json['sandbox_id']['$oid'];
    $business_type=$line_json['business_type'];
    $active_environment=$line_json['active_environment'];
    $fee=json_encode($line_json['fee']);

    $sql= "INSERT INTO businesses (`id`, `merchant_id`, `linked_users`, `name`, `category`, `key_pairs`, `webhook_url`, `allowed_to_go_live`, `instant_settlement`, `countries`, `skip_commit_stage`, `pos_transaction_limit`, `user_invites`, `reseller_invites`, `remittance_config`, `email_settings`, `limits`, `disable_settlements`, `timestamp`, `settlement_period`, `phonenumber`, `business_type`, `sandbox_id`, `active_environment`, `fee` ) VALUES ('${id}','${merchant_id}','${linked_users}','${name}','${category}','${key_pairs}','${webhook_url}','${allowed_to_go_live}','${instant_settlement}','${countries}','${skip_commit_stage}','${pos_transaction_limit}','${user_invites}','${reseller_invites}','${remittance_config}','${email_settings}','${limits}','${disable_settlements}','${timestamp}','${settlement_period}','${phonenumber}','${business_type}','${sandbox_id}','${active_environment}','${fee}');";
    //echo $sql."\n";
    @error_log($sql."\n",3,$out_filename);

    #################key_pairs为数组，关联表businesses_merchant_key商户秘钥表
    $key_pairs=$line_json['key_pairs'];
    if(!empty($key_pairs) && is_array($key_pairs)){
        foreach ($key_pairs as $k=>$v) {
            $public_key=$v['public_key'];
            $private_key_cipher_text=$v['private_key']['cipher_text']['$binary'];
            $private_key_mac=$v['private_key']['mac']['$binary'];
            $key_env=$v['tag'];

            $sql= "INSERT INTO businesses_merchant_key (`id`, `merchant_id`, `public_key`, `private_key_cipher_text`, `private_key_mac`, `key_env` ) VALUES ('${id}','${merchant_id}','${public_key}','${private_key_cipher_text}','${private_key_mac}','${key_env}');";
            //echo $sql."\n";
            @error_log($sql."\n",3,$out_filename);
        }
    }

    ####businesses_merchant_email_settings 表
    $email_settings=$line_json['email_settings'];
    $enable_transaction_notifications=$line_json['email_settings']['enable_transaction_notifications']?"1":"0";
    $enable_transfer_notifications=$line_json['email_settings']['enable_transfer_notifications']?"1":"0";
    $enable_low_balance_notifications=$line_json['email_settings']['enable_low_balance_notifications']?"1":"0";
    $balance_limit_value=$line_json['email_settings']['balance_limit']['value'];
    $balance_limit_floatvalue=$line_json['email_settings']['balance_limit']['floatValue'];
    $balance_limit_currency=$line_json['email_settings']['balance_limit']['currency'];
    $balance_notification_sent=$line_json['email_settings']['balance_notification_sent']?"1":"0";

    if(!empty($email_settings)){
        $sql= "INSERT INTO businesses_merchant_email_settings (`id`, `merchant_id`, `enable_transaction_notifications`, `enable_transfer_notifications`, `enable_low_balance_notifications`, `balance_limit_value`, `balance_limit_floatvalue`, `balance_limit_currency`, `balance_notification_sent` ) VALUES ('${id}','${merchant_id}','${enable_transaction_notifications}','${enable_transfer_notifications}','${enable_low_balance_notifications}','${balance_limit_value}','${balance_limit_floatvalue}','${balance_limit_currency}','${balance_notification_sent}');";
        //echo $sql."\n";
        @error_log($sql."\n",3,$out_filename);
    }


    #################reseller_invites为数组，关联表businesses_merchant_reseller_invites商户邀请代理商表
    $reseller_invites=$line_json['reseller_invites'];
    if(!empty($reseller_invites) && is_array($reseller_invites)) {
        foreach ($reseller_invites as $k => $v) {
            $reseller_invites_id = $v['_id']['$oid'];
            $email = addslashes($v['email']);
            $token = $v['token'];
            $created_at = str_replace("Z", "", str_replace("T", " ", $v['createdAt']['$date']));

            $sql = "INSERT INTO businesses_merchant_reseller_invites (`id`, `merchant_id`, `email`, `token`, `created_at` ) VALUES ('${reseller_invites_id}','${merchant_id}','${email}','${token}','${created_at}');";
            //echo $sql."\n";
            @error_log($sql . "\n", 3, $out_filename);
        }
    }


    #################user_invites为数组，关联表businesses_merchant_user_invites邀请用户表
    $user_invites=$line_json['user_invites'];
    if(!empty($user_invites) && is_array($user_invites)) {
        foreach ($user_invites as $k => $v) {
            $token = $v['token'];
            $email = addslashes($v['email']);
            $created_at = str_replace("Z", "", str_replace("T", " ", $v['created_at']['$date']));
            $expired = $v['expired'] ? "1" : "0";
            $extra = $v['extra'];

            $sql = "INSERT INTO businesses_merchant_user_invites (`id`, `merchant_id`, `token`, `email`, `created_at`, `expired`, `extra` ) VALUES ('${id}','${merchant_id}','${token}','${email}','${created_at}','${expired}','${extra}');";
            //echo $sql."\n";
            @error_log($sql . "\n", 3, $out_filename);
        }
    }


    #################business_aggregators
    $aggregators=$line_json['aggregators'];
    if(!empty($aggregators) && is_array($aggregators)) {
        foreach ($aggregators as $k => $v) {
            $business_id = ${id};
            $authority_id = $v['authority']['_id']['$oid'];
            $authority_merchant_id = $v['authority']['business']['merchant_id']['$oid'];
            $authority_version = $v['authority']['version'];
            $authority_type = $v['authority']['type'];
            $status = $v['status'];
            $tier = $v['tier'];
            $commission = json_encode($v['commission']);
            $created_at = str_replace("Z", "", str_replace("T", " ", $v['created_at']['$date']));

            $sql = "INSERT INTO business_aggregators (`business_id`, `authority_id`, `authority_version`, `authority_type`, `status`, `tier`, `commission`, `created_at` ) VALUES ('${business_id}','${authority_id}','${authority_version}','${authority_type}','${status}','${tier}','${commission}','${created_at}');";
            //echo $sql."\n";
            @error_log($sql . "\n", 3, $out_filename);
        }
    }
}
?>