#!/usr/bin/php
<?php
define('DS', DIRECTORY_SEPARATOR);
define('ROOT_PATH', realpath(dirname(__FILE__).DS).DS);
include_once (ROOT_PATH."global.php");

$file = @file($in_filename);
foreach($file as $line)
{
    //echo $line;
    $line_json = json_decode($line,true);
    if(!is_array($line_json)){
        @error_log($line."\n",3,"/tmp/mongodb_mysql_error.log");
        continue;
    }
    //var_dump($line_json);
    $id=$line_json['_id']['$oid'];
    $user_id=$line_json['user_id']['$oid'];
    $payment_id=$line_json['payment_id'];
    $number=$line_json['number'];
    $scheme=$line_json['scheme'];
    $type=$line_json['type'];
    $account_number=$line_json['account_number'];
    $signature=$line_json['signature'];
    $signature_version=$line_json['signature_version'];
    $data_cipher_text=$line_json['data']['cipher_text']['$binary'];
    $data_mac=$line_json['data']['mac']['$binary'];
    $created=str_replace("Z","",str_replace("T"," ",$line_json['created']['$date']));
    $updated=str_replace("Z","",str_replace("T"," ",$line_json['updated']['$date']));
    $deleted=str_replace("Z","",str_replace("T"," ",$line_json['deleted']['$date']));

    if(!empty($id)) {
        $sql_start = "INSERT";
        if($sql_type=="REPLACE"){
            $sql_start = "REPLACE";
        }
        $sql = "$sql_start INTO payment_instruments (`id`, `user_id`, `payment_id`, `number`, `scheme`, `type`, `account_number`, `signature`, `signature_version`, `data_cipher_text`, `data_mac`, `created`, `updated`, `deleted` ) VALUES ('${id}','${user_id}','${payment_id}','${number}','${scheme}','${type}','${account_number}','${signature}','${signature_version}','${data_cipher_text}','${data_mac}','${created}','${updated}','${deleted}');";
        //echo $sql."\n";
        @error_log($sql . "\n", 3, $out_filename);
    }
}
?>
