#!/usr/bin/php
<?php
define('DS', DIRECTORY_SEPARATOR);
define('ROOT_PATH', realpath(dirname(__FILE__).DS).DS);
include_once (ROOT_PATH."global.php");

$file = @file($in_filename);
foreach($file as $line)
{
    //echo $line;
    $line_json = json_decode($line,true);
    if(!is_array($line_json)){
        @error_log($line."\n",3,"/tmp/mongodb_mysql_error.log");
        continue;
    }
    //var_dump($line_json);
    ####user 表
    $id=$line_json['_id']['$oid'];
    $name=$line_json['name'];
    $create_date=str_replace("Z","",str_replace("T"," ",$line_json['create_date']['$date']));
    $last_visit=str_replace("Z","",str_replace("T"," ",$line_json['last_visit']['$date']));
    $phonenumber=$line_json['phonenumber'];
    $email_verified=$line_json['email_verified']?"1":"0";
    $email=addslashes($line_json['email']);
    $hash=$line_json['pin']['hash']['$binary'];
    $salt=$line_json['pin']['salt']['$binary'];
    $kyc_level=$line_json['kyc_level'];
    $upgraded_date=str_replace("Z","",str_replace("T"," ",$line_json['upgraded_date']['$date']));
    $role=$line_json['role'];
    $referral_code=$line_json['referral_code'];
    $referrer_code=$line_json['referrer_code'];
    $limit=json_encode($line_json['limit']);

    $sql="INSERT INTO user (`id`, `name`, `create_date`, `last_visit`, `phonenumber`, `email_verified`, `email`, `hash`, `salt`, `kyc_level`, `upgraded_date`, `role`, `referral_code`, `referrer_code`, `limit` ) VALUES ('${id}',\"${name}\",'${create_date}','${last_visit}','${phonenumber}','${email_verified}','${email}','${hash}','${salt}','${kyc_level}','${upgraded_date}','${role}','${referral_code}','${referrer_code}','${limit}');";
    //echo $sql."\n";
    @error_log($sql."\n",3,$out_filename);

    ####user_detail 表
    $first_name=addslashes($line_json['first_name']);
    $middle_name=addslashes($line_json['middle_name']);
    $business_name=addslashes($line_json['business_name']);
    $surname=addslashes($line_json['surname']);
    $dob=$line_json['dob'];
    $email_to_verify=addslashes($line_json['email_to_verify']);
    $email_verification_id=$line_json['email_verification_id']['$oid'];
    $email_verification_date=str_replace("Z","",str_replace("T"," ",$line_json['email_verification_date']['$date']));
    $email_verification_cooldown=str_replace("Z","",str_replace("T"," ",$line_json['email_verification_cooldown']['$date']));
    $email_verification_last_sent=str_replace("Z","",str_replace("T"," ",$line_json['email_verification_last_sent']['$date']));
    $verification_emails_sent=$line_json['verification_emails_sent'];
    $address=addslashes($line_json['address']);
    $verified=$line_json['verified']?"1":"0";
    $verified_by_id=$line_json['verified_by']['_id']['$oid'];
    $verification_date=str_replace("Z","",str_replace("T"," ",$line_json['upgraded_date']['$date']));
    $gender=$line_json['gender'];
    $state=$line_json['state'];
    $country=$line_json['country'];
    $city=addslashes($line_json['city']);
    $lga=addslashes($line_json['lga']);
    $migrated_aggregator=$line_json['migrated_aggregator']?"1":"0";
    $limits=json_encode($line_json['limits']);
    $terms_and_conditions_config=json_encode($line_json['terms_and_conditions_config']);

    $sql="INSERT INTO user_detail (`id`, `first_name`, `middle_name`, `business_name`, `surname`, `dob`, `email_to_verify`, `email_verification_id`, `email_verification_date`, `email_verification_cooldown`, `email_verification_last_sent`, `verification_emails_sent`, `address`, `verified`, `verified_by_id`, `verification_date`, `gender`, `state`, `country`, `city`, `lga`, `migrated_aggregator`, `limits`, `terms_and_conditions_config` ) VALUES ('${id}',\"${first_name}\",\"${middle_name}\",\"${business_name}\",\"${surname}\",'${dob}','${email_to_verify}','${email_verification_id}','${email_verification_date}','${email_verification_cooldown}','${email_verification_last_sent}','${verification_emails_sent}',\"${address}\",'${verified}','${verified_by_id}','${verification_date}','${gender}','${state}','${country}',\"${city}\",\"${lga}\",'${migrated_aggregator}','${limits}','${terms_and_conditions_config}');";
    //echo $sql."\n";
    @error_log($sql."\n",3,$out_filename);

    ####kyc_upload 写document表
    $kyc_upload=$line_json['kyc_upload'];
    if(!empty($kyc_upload) && is_array($kyc_upload)){
        foreach ($kyc_upload as $k=>$v){
            foreach ($v as $vv){
                $document_id=$vv['_id']['$oid'];
                $user_id=${id};
                $type=${k};
                $file=$vv['file'];
                $filename=addslashes($vv['filename']);
                $deleted=$vv['deleted']?"1":"0";
                $size=$vv['size'];
                $upload_date=str_replace("Z","",str_replace("T"," ",$vv['upload_date']['$date']));
                $status=$vv['status'];
                $reject_reason=addslashes($vv['reject_reason']);

                if(!empty($document_id))
                {
                    $sql="INSERT INTO document (`id`, `user_id`, `type`, `file`, `filename`, `deleted`, `size`, `upload_date`, `status`, `reject_reason` ) VALUES ('${document_id}','${user_id}','${type}','${file}',\"${filename}\",'${deleted}','${size}','${upload_date}','${status}',\"${reject_reason}\");";
                    //echo $sql."\n";
                    @error_log($sql."\n",3,$out_filename);
                }
            }
        }
    }

    ####user_bvn 表
    $bvn=$line_json['bvn'];
    if(!empty($bvn)) {

        ####bvn 里的document
        $document_id=$line_json['bvn']['image']['_id']['$oid'];
        $user_id=${id};
        $type="bvn";
        $file=$line_json['bvn']['image']['file'];
        $filename=addslashes($line_json['bvn']['image']['filename']);
        $deleted=$line_json['bvn']['image']['deleted']?"1":"0";
        $size=$line_json['bvn']['image']['size'];
        $upload_date=str_replace("Z","",str_replace("T"," ",$line_json['bvn']['image']['upload_date']['$date']));
        $status=$line_json['bvn']['image']['status'];
        $reject_reason=addslashes($line_json['bvn']['image']['reject_reason']);

        if(!empty($document_id))
        {
            $sql="INSERT INTO document (`id`, `user_id`, `type`, `file`, `filename`, `deleted`, `size`, `upload_date`, `status`, `reject_reason` ) VALUES ('${document_id}','${user_id}','${type}','${file}',\"${filename}\",'${deleted}','${size}','${upload_date}','${status}','${reject_reason}');";
            //echo $sql."\n";
            @error_log($sql."\n",3,$out_filename);
        }

        $number = addslashes($line_json['bvn']['number']);
        $provider = $line_json['bvn']['provider'];
        $transactionid = $line_json['bvn']['transactionid'];
        $first_name = addslashes($line_json['bvn']['first_name']);
        $middle_name = addslashes($line_json['bvn']['middle_name']);
        $last_name = addslashes($line_json['bvn']['last_name']);
        $full_name = addslashes($line_json['bvn']['full_name']);
        $phonenumber = $line_json['bvn']['phonenumber'];
        $dob = $line_json['bvn']['dob'];
        $gender = $line_json['bvn']['gender'];
        $address = addslashes($line_json['bvn']['address']);
        $image_id = ${document_id};

        $sql= "INSERT INTO user_bvn (`id`, `number`, `provider`, `transactionid`, `first_name`, `middle_name`, `last_name`, `full_name`, `phonenumber`, `dob`, `gender`, `address`, `image_id` ) VALUES ('${id}','${number}','${provider}','${transactionid}',\"${first_name}\",\"${middle_name}\",\"${last_name}\",\"${full_name}\",'${phonenumber}','${dob}','${gender}',\"${address}\",'${image_id}');";
        //echo $sql."\n";
        @error_log($sql."\n",3,$out_filename);
    }

    ####user_identification 表
    $identification=$line_json['identification'];
    if(!empty($identification)) {

        $type = $line_json['identification']['type'];
        $number = $line_json['identification']['number'];

        $sql= "INSERT INTO user_identification  (`id`, `type`, `number` ) VALUES ('${identification_id}','${type}','${number}');";
        //echo $sql."\n";
        @error_log($sql."\n",3,$out_filename);
    }

    ####user_linked_users 表
    $linked_users=$line_json['linked_users'];
    if(!empty($linked_users)) {
        $user_id = ${id};
        $linked_user_id = $line_json['linked_users']['linked_user_id'];

        if(!empty($linked_user_id)){
            $sql= "INSERT INTO user_linked_users (`user_id`, `linked_user_id` ) VALUES ('${user_id}','${linked_user_id}');";
            //echo $sql."\n";
            @error_log($sql."\n",3,$out_filename);
        }
    }


    ####user_reseller_invite 表
    $reseller_invites=$line_json['reseller_invites'];
    if(!empty($reseller_invites)) {
        $user_id = ${id};
        $email = $line_json['reseller_invites']['email'];
        $token = $line_json['reseller_invites']['token'];
        $created_at =str_replace("Z","",str_replace("T"," ",$line_json['bvn']['image']['created_at']['$date']));

        $sql="INSERT INTO user_reseller_invite (`id`, `user_id`, `email`, `token`, `created_at` ) VALUES ('${id}','${user_id}','${email}','${token}','${created_at}');";
        //echo $sql."\n";
        @error_log($sql."\n",3,$out_filename);
    }

    ####user_service_config 表
    $service_config=$line_json['service_config'];
    if(!empty($service_config)) {
        $user_id = ${id};
        $name = $line_json['service_config']['ewithdrawal']['name'];
        $enable = $line_json['service_config']['ewithdrawal']['enabled'] ? "1" : "0";
        $config = $line_json['service_config']['ewithdrawal']['config'];
        $service_id = "ewithdrawal";

        if(!empty($line_json['service_config']['ewithdrawal'])){
            $sql = "INSERT INTO user_service_config (`id`, `user_id`, `name`, `enable`, `config`, `service_id` ) VALUES ('${id}','${user_id}','${name}','${enable}','${config}','${service_id}');";
            //echo $sql . "\n";
            @error_log($sql . "\n", 3, $out_filename);
        }

        $name = $line_json['service_config']['investment-interest']['name'];
        $enable = $line_json['service_config']['investment-interest']['enabled'] ? "1" : "0";
        $config = $line_json['service_config']['investment-interest']['config'];
        $service_id = "investment-interest";

        if(!empty($line_json['service_config']['investment-interest'])) {
            $sql = "INSERT INTO user_service_config (`id`, `user_id`, `name`, `enable`, `config`, `service_id` ) VALUES ('${id}','${user_id}','${name}','${enable}','${config}','${service_id}');";
            //echo $sql . "\n";
            @error_log($sql . "\n", 3, $out_filename);
        }
    }


    ####user_telesale 表
    $telesale=$line_json['telesale'];
    if(!empty($telesale)) {
        $number = $line_json['telesale']['number'];
        $team = $line_json['telesale']['team'];

        $sql = "INSERT INTO user_telesale (`id`, `number`, `team` ) VALUES ('${id}','${number}','${team}');";
        //echo $sql . "\n";
        @error_log($sql . "\n", 3, $out_filename);
    }

    ####user_upgrade 表
    $user_upgrade=$line_json['user_upgrade'];
    if(!empty($user_upgrade)) {
        $user_id = ${id};
        $role = $line_json['user_upgrade']['role'];
        $accept_by = $line_json['user_upgrade']['accept_by']['_id']['$oid'];
        $document_confirmed_by = $line_json['user_upgrade']['document_confirmed_by']['_id']['$oid'];
        $document_confirmed_date = str_replace("Z", "", str_replace("T", " ", $line_json['user_upgrade']['document_confirmed_date']['$date']));
        $upgrade_status = $line_json['user_upgrade']['upgrade_status'];
        $doc_status = $line_json['user_upgrade']['doc_status'];
        $upgrade_submit_date = str_replace("Z", "", str_replace("T", " ", $line_json['user_upgrade']['upgrade_submit_date']['$date']));

        $sql = "INSERT INTO user_upgrade (`id`, `user_id`, `role`, `accept_by`, `document_confirmed_by`, `document_confirmed_date`, `upgrade_status`, `doc_status`, `upgrade_submit_date` ) VALUES ('${id}','${user_id}','${role}','${accept_by}','${document_confirmed_by}','${document_confirmed_date}','${upgrade_status}','${doc_status}','${upgrade_submit_date}');";
        //echo $sql . "\n";
        @error_log($sql . "\n", 3, $out_filename);
    }

    ####user_aggregators 表，有二个节点
    $aggregators=$line_json['aggregators'];
    if(!empty($aggregators) && is_array($aggregators)) {
        foreach ($aggregators as $k => $v) {
            $user_id = ${id};
            $update_by = $v['authority']['update_by'];
            $authority_id = $v['authority']['_id']['$oid'];
            $authority_version = $v['authority']['version'];
            $authority_type = $v['authority']['type'];
            $status = $v['authority']['status'];
            $tier = $v['authority']['tier'];
            $commission = json_encode($v['commission']);
            $created_at = str_replace("Z", "", str_replace("T", " ", $v['created_at']['$date']));
            $aggregators_type="aggregators";

            $sql = "INSERT INTO user_aggregators (`user_id`, `update_by`, `authority_id`, `authority_version`, `authority_type`, `status`, `tier`, `commission`, `created_at`,`aggregators_type` ) VALUES ('${user_id}','${update_by}','${authority_id}','${authority_version}','${authority_type}','${status}','${tier}','${commission}','${created_at}','${aggregators_type}');";
            //echo $sql . "\n";
            @error_log($sql . "\n", 3, $out_filename);
        }
    }
    $legacy_aggregators=$line_json['legacy_aggregators'];
    if(!empty($legacy_aggregators) && is_array($legacy_aggregators)) {
        foreach ($legacy_aggregators as $k => $v) {
            $user_id = ${id};
            $update_by = $v['authority']['update_by'];
            $authority_id = $v['authority']['_id']['$oid'];
            $authority_version = $v['authority']['version'];
            $authority_type = $v['authority']['type'];
            $status = $v['authority']['status'];
            $tier = $v['authority']['tier'];
            $commission = json_encode($v['commission']);
            $created_at = str_replace("Z", "", str_replace("T", " ", $v['created_at']['$date']));
            $aggregators_type="legacy_aggregators";

            $sql = "INSERT INTO user_aggregators (`user_id`, `update_by`, `authority_id`, `authority_version`, `authority_type`, `status`, `tier`, `commission`, `created_at`,`aggregators_type` ) VALUES ('${user_id}','${update_by}','${authority_id}','${authority_version}','${authority_type}','${status}','${tier}','${commission}','${created_at}','${aggregators_type}');";
            //echo $sql . "\n";
            @error_log($sql . "\n", 3, $out_filename);
        }
    }
}
?>