#!/usr/bin/php
<?php
define('DS', DIRECTORY_SEPARATOR);
define('ROOT_PATH', realpath(dirname(__FILE__).DS).DS);
include_once (ROOT_PATH."global.php");

$file = @file($in_filename);
foreach($file as $line)
{
    //echo $line;
    $line_json = json_decode($line,true);
    if(!is_array($line_json)){
        @error_log($line."\n",3,"/tmp/mongodb_mysql_error.log");
        continue;
    }
    //var_dump($line_json);
    $id=$line_json['_id']['$oid'];
    $version=$line_json['version'];
    $signature=$line_json['signature']['$binary'];
    $transactions=$line_json['transactions'];

    //增量数据处理开始日期
    $startbyday = trim($argv[4]);

    if(!empty($transactions) && is_array($transactions)){
        foreach ($transactions as $k=>$v) {
            $transaction = $v;

            $transaction_timestamp = str_replace("Z", "", str_replace("T", " ", $transaction['transaction']['timestamp']['$date']));

            //如果是增量处理，则只处理时间大于增量时间的交易记录
            if(!empty($startbyday)){
                if (strtotime($transaction_timestamp) < strtotime($startbyday." 00:00:00")){
                    continue;
                }
            }

            $transaction_id = $transaction['transaction']['_id']['$oid'];
            $transaction_version = $transaction['transaction']['version'];
            $transaction_incoming = $transaction['transaction']['incoming'] ? "1" : "0";
            $transaction_opcode = $transaction['transaction']['opcode'];
            $transaction_type = $transaction['transaction']['type'];
            $transaction_account = $transaction['transaction']['account'];
            $transaction_description = $transaction['transaction']['description'];
            $transaction_ext_ref = $transaction['transaction']['ext_ref'];
            $transaction_phone_number = $transaction['transaction']['phone_number'];
            $transaction_currency_amount_value = $transaction['transaction']['currency_amount']['value'];
            $transaction_currency_amount_floatValue = $transaction['transaction']['currency_amount']['floatValue'];
            $transaction_currency_amount_currency = $transaction['transaction']['currency_amount']['currency'];
            $transaction_currency_amount = json_encode($transaction['transaction']['currency_amount']);
            $transaction_balance_after = json_encode($transaction['transaction']['balance_after']);

            $transaction_string = $transaction['string'];
            $transaction_meta = "";//此值暂无用 json_encode($transaction['meta']);
            $transaction_signature = "";//此值暂无用 $transaction['signature']['$binary'];

            if(!empty($id)) {
                $sql_start="INSERT";
                if($sql_type=="REPLACE"){
                    $sql_start="REPLACE";
                }
                $sql = "$sql_start INTO chain_boxes (`id`, `version`, `signature`, `transaction_id`, `transaction_version`, `transaction_incoming`, `transaction_opcode`, `transaction_type`, `transaction_account`, `transaction_timestamp`, `transaction_description`, `transaction_ext_ref`, `transaction_phone_number`, `transaction_currency_amount_value`, `transaction_currency_amount_floatValue`, `transaction_currency_amount_currency`, `transaction_string`, `transaction_meta`, `transaction_signature`, `transaction_currency_amount`, `transaction_balance_after` ) VALUES ('${id}','${version}','${signature}','${transaction_id}','${transaction_version}','${transaction_incoming}','${transaction_opcode}','${transaction_type}','${transaction_account}','${transaction_timestamp}','${transaction_description}','${transaction_ext_ref}','${transaction_phone_number}','${transaction_currency_amount_value}','${transaction_currency_amount_floatValue}','${transaction_currency_amount_currency}','${transaction_string}','${transaction_meta}','${transaction_signature}','${transaction_currency_amount}','${transaction_balance_after}');";
                //echo $sql."\n";
                @error_log($sql . "\n", 3, $out_filename);
            }
        }
    }
}
?>
