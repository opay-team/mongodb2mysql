#!/bin/sh
#utc 时间1点10分处理数据更新
#10 1 * * * sh /data/hangjun.mao/work/crontab.sh > /dev/null 2>&1

export DATA_SHELL_PATH=$(cd "$(dirname "$0")"; pwd)
source ${DATA_SHELL_PATH}/config/Config.sh

cd ${DATA_SHELL_PATH}

sh ${DATA_SHELL_PATH}/fees.sh > ${DATA_SHELL_PATH}/tmp/fees.log &
sh ${DATA_SHELL_PATH}/blacklisted_instrument.sh > ${DATA_SHELL_PATH}/tmp/blacklisted_instrument.log &
sh ${DATA_SHELL_PATH}/financial_institutions.sh > ${DATA_SHELL_PATH}/tmp/financial_institutions.log &
sh ${DATA_SHELL_PATH}/configuration.sh > ${DATA_SHELL_PATH}/tmp/configuration.log &
sh ${DATA_SHELL_PATH}/overlord_users.sh > ${DATA_SHELL_PATH}/tmp/overlord_users.log &
sh ${DATA_SHELL_PATH}/terminals.sh > ${DATA_SHELL_PATH}/tmp/terminals.log &
sh ${DATA_SHELL_PATH}/terminal_owners.sh > ${DATA_SHELL_PATH}/tmp/terminal_owners.log &
sh ${DATA_SHELL_PATH}/payment_terminal_providers.sh > ${DATA_SHELL_PATH}/tmp/payment_terminal_providers.log &
sh ${DATA_SHELL_PATH}/merchant.sh > ${DATA_SHELL_PATH}/tmp/merchant.log &
sh ${DATA_SHELL_PATH}/businesses.sh > ${DATA_SHELL_PATH}/tmp/businesses.log &
sh ${DATA_SHELL_PATH}/business_users.sh > ${DATA_SHELL_PATH}/tmp/business_users.log &

sh ${DATA_SHELL_PATH}/user.sh > ${DATA_SHELL_PATH}/tmp/user.log &

#每天凌晨入库及更新昨天0点到当前的数据
y=`date +%Y --date='1 day ago'`
m=`date +%m --date='1 day ago'`
d=`date +%d --date='1 day ago'`

echo "开始处理：${y} ${m} ${d} 增量更新数据" >> ${DATA_SHELL_PATH}/tmp/byday.log

sh ${DATA_SHELL_PATH}/payment_instruments_byday.sh ${y} ${m} ${d} > ${DATA_SHELL_PATH}/tmp/payment_instruments_byday.log &
sh ${DATA_SHELL_PATH}/coins_transactions_byday.sh ${y} ${m} ${d} > ${DATA_SHELL_PATH}/tmp/coins_transactions_byday.log &
sh ${DATA_SHELL_PATH}/chains_byday.sh ${y} ${m} ${d} > ${DATA_SHELL_PATH}/tmp/chains_byday.log &
sh ${DATA_SHELL_PATH}/chain_boxes_byday.sh ${y} ${m} ${d} > ${DATA_SHELL_PATH}/tmp/chain_boxes_byday.log &
