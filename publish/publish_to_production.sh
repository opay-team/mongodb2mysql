#!/bin/sh

export PROJECT_CODE_PATH=$(cd "$(dirname "$0")/../"; pwd)

scp ${PROJECT_CODE_PATH}/*.sh root@10.1.2.58:/data/hangjun.mao/work/
scp ${PROJECT_CODE_PATH}/*.php root@10.1.2.58:/data/hangjun.mao/work/
scp ${PROJECT_CODE_PATH}/decrypt_production root@10.1.2.58:/data/hangjun.mao/work/

scp ${PROJECT_CODE_PATH}/opay.sql root@10.1.2.58:/data/hangjun.mao/

