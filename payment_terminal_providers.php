#!/usr/bin/php
<?php
define('DS', DIRECTORY_SEPARATOR);
define('ROOT_PATH', realpath(dirname(__FILE__).DS).DS);
include_once (ROOT_PATH."global.php");

$file = @file($in_filename);
foreach($file as $line)
{
    //echo $line;
    $line_json = json_decode($line,true);
    if(!is_array($line_json)){
        @error_log($line."\n",3,"/tmp/mongodb_mysql_error.log");
        continue;
    }
    //var_dump($line_json);
    $id=$line_json['_id']['$oid'];
    $name=$line_json['name'];
    $authorization_hash_hash=$line_json['authorization_hash']['hash']['$binary'];
    $authorization_hash_salt=$line_json['authorization_hash']['salt']['$binary'];
    $country=$line_json['country'];
    $created=str_replace("Z","",str_replace("T"," ",$line_json['created']['$date']));
    $authorization=$line_json['authorization'];
    $password=$line_json['password'];

    $sql="INSERT INTO payment_terminal_providers (`id`, `name`, `authorization_hash_hash`, `authorization_hash_salt`, `country`, `created`, `authorization`, `password` ) VALUES ('${id}','${name}','${authorization_hash_hash}','${authorization_hash_salt}','${country}','${created}','${authorization}','${password}');";
    //echo $sql."\n";
    @error_log($sql."\n",3,$out_filename);
}
?>
