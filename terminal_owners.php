#!/usr/bin/php
<?php
define('DS', DIRECTORY_SEPARATOR);
define('ROOT_PATH', realpath(dirname(__FILE__).DS).DS);
include_once (ROOT_PATH."global.php");

$file = @file($in_filename);
foreach($file as $line)
{
    //echo $line;
    $line_json = json_decode($line,true);
    if(!is_array($line_json)){
        @error_log($line."\n",3,"/tmp/mongodb_mysql_error.log");
        continue;
    }
    //var_dump($line_json);
    $id=$line_json['_id']['$oid'];
    $terminal_id=$line_json['terminal_id']['$oid'];
    $pos_id=$line_json['pos_id'];
    $serial=$line_json['serial'];
    $owner_id=$line_json['owner_id']['$oid'];
    $owner_name=addslashes($line_json['owner_name']);
    $owner_type=$line_json['owner_type'];
    $created=str_replace("Z","",str_replace("T"," ",$line_json['created']['$date']));

    $sql= "INSERT INTO terminal_owners (`id`, `terminal_id`, `pos_id`, `serial`, `owner_id`, `owner_name`, `owner_type`, `created` ) VALUES ('${id}','${terminal_id}','${pos_id}','${serial}','${owner_id}','${owner_name}','${owner_type}','${created}');";
    //echo $sql."\n";
    @error_log($sql."\n",3,$out_filename);
}
?>
