#!/bin/sh
#检查用户OWealth余额正确性支持V2/V3查询
#数据来源Mysql
#前提是需要先完成余额表数据的同步。用mongodb balance_after的比较准确

export DATA_SHELL_PATH=$(cd "$(dirname "$0")"; pwd)
source ${DATA_SHELL_PATH}/config/Config.sh

usage() {
    echo "Usage: sh $0 type phonenumber"
    echo "Example: sh $0 [id|phonenumber] +2347060784311"
    echo "sh $0 phonenumber +2347060784311"
    echo "sh $0 id 5a4c6652c94e4800012f89ca"
    exit 1
}

if [ $# != 2 ]; then
    usage
fi

FIELD_VALUE=$2
FIELD=$1

if [ "${FIELD}" == "id" ];then
    user_id=`${BIN_MYSQL} --skip-column-names -e "SELECT id FROM ${DB_NAME}.user WHERE id='${FIELD_VALUE}'" 2>/dev/null`
else
    user_id=`${BIN_MYSQL} --skip-column-names -e "SELECT id FROM ${DB_NAME}.user WHERE phonenumber='${FIELD_VALUE}'" 2>/dev/null`
fi

if [ "${user_id}" = "" ];then
    echo "user not found."
    exit
fi

main_data_cipher_text=`${BIN_MYSQL} --skip-column-names -e "SELECT data_cipher_text FROM ${DB_NAME}.payment_instruments WHERE user_id='${user_id}' and scheme='Investment' limit 1" 2>/dev/null`

#解密
json_string=`${DATA_SHELL_PATH}/decrypt_production -text="${main_data_cipher_text}"`

box_name=`echo "${json_string}" | jq -r -c '.box_name'`
chain_id=`echo "${json_string}" | jq -r -c '.chain_id'`

if [ "${chain_id}" != "" ] && [ "${chain_id}" != "null" ] ;then
    balance_after=`${BIN_MYSQL} --skip-column-names -e "SELECT balance_after FROM ${DB_NAME}.coins_transactions WHERE chain_id='${chain_id}' order by counter desc limit 1" 2>/dev/null`
    amount=`echo "${balance_after}" | jq -r -c '.available_balance.value'`
    available_balance=`echo "${balance_after}" | jq -c '.available_balance'`
    echo -e "MysqlV3:\t${user_id}\t${amount}\t${chain_id}\t${available_balance}"
else
    if [ "${box_name}" != "" ] && [ "${box_name}" != "null" ] ;then
        amount=`${BIN_MYSQL} --skip-column-names -e "select round(sum(if(transaction_incoming=0,-1*transaction_currency_amount_value,transaction_currency_amount_value)),3) amount from (select chains.id box_name,chain_boxes.transaction_id,max(chain_boxes.transaction_incoming) transaction_incoming,max(chain_boxes.transaction_currency_amount_value) transaction_currency_amount_value from ${DB_NAME}.chain_boxes chain_boxes  inner join  ${DB_NAME}.chains on chain_boxes.id=chains.box_id where chains.id in ('${box_name}') and chain_boxes.transaction_account='investment' and chain_boxes.transaction_currency_amount_currency='NGN' group by chains.id ,chain_boxes.transaction_id) t1 group by box_name" 2>/dev/null`
        echo -e "MysqlV2:\t${user_id}\t${amount}\t${box_name}\t${transaction_balance_after}"
    fi
fi