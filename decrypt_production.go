package main
//CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build decrypt_production.go

import (
	"crypto/aes"
	"crypto/cipher"
	"encoding/base64"
	"flag"
	"fmt"
	"os"
)

type EncryptedData struct {
	CipherText []byte `bson:"cipher_text" json:"cipher_text"`
}

type jsonString string

func (self EncryptedData) DecryptTo(encryptKey []byte) (string,error) {
	block, err := aes.NewCipher(encryptKey)
	if err != nil {
		return "",err
	}

	body := make([]byte, len(self.CipherText)-aes.BlockSize)

	/* IV is stored in the first aes.BlockSize bytes of data */
	stream := cipher.NewCTR(block, self.CipherText[:aes.BlockSize])
	stream.XORKeyStream(body, self.CipherText[aes.BlockSize:])

	jsonString := string(body)

	return jsonString,err
}

func main(){
	//测试环境
	//encryptKey := []byte("MCDe2hNoLngoT86iadNvigAD6x5ezwqr")
	//正式环境KEY
	encryptKey := []byte("")

	text := flag.String("text", "", "./decrypt -text=''")

	flag.Parse()
	//fmt.Println("text:", *text)

	cipher_text:=*text
	if( cipher_text == "" || len(cipher_text) < 16 ){
		//cipher_text = "3sEj4JvbDfiVnr55Ec8i6yLt89LglxujV3PSE+EHiNtueX4TpwxYLlOx3NHg5NUNUVDZNOhmeGvZWofCQsAsES73Ue+h424qt+XTtRKmcttZtJ8zqrRu/SRseX8agFHyCC3Gps4aUiT8GELiJFcnEx1/tvIKuKgj7hMtHgDY3457qDZ9VA=="
		//cipher_text = "7xGczpbxfptHiAuE50nDcVt21xhHBPNZsgGNt1y06OtErg9CCq3GAKuiVt3XXjpzhQe+hXdbuQ0+k2qemFh1bOd/qON5oioojyVWzQT/de789DhIDHZ2mnAILxdgpF4hNiLHH9OeWobPJN18u+mx29/g1I4qwbnUidu9oW9Svw=="
		fmt.Println("cipher_text is error")
		os.Exit(0)
	}
	decodeCipherText, _ := base64.StdEncoding.DecodeString(cipher_text)
	encryptedData:=EncryptedData{
		CipherText: decodeCipherText,
	}

	json,_:=encryptedData.DecryptTo(encryptKey)

	fmt.Println(json)
}