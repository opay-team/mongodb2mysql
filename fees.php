#!/usr/bin/php
<?php
define('DS', DIRECTORY_SEPARATOR);
define('ROOT_PATH', realpath(dirname(__FILE__).DS).DS);
include_once (ROOT_PATH."global.php");

$file = @file($in_filename);
foreach($file as $line)
{
    //echo $line;
    $line_json = json_decode($line,true);
    if(!is_array($line_json)){
        @error_log($line."\n",3,"/tmp/mongodb_mysql_error.log");
        continue;
    }
    //var_dump($line_json);
    $id=$line_json['_id']['$oid'];
    $instrument_type=$line_json['instrument_type'];
    $service_type=$line_json['service_type'];
    $config_type=$line_json['config_type'];
    $service_id=$line_json['service_id'];
    $date_created=str_replace("Z","",str_replace("T"," ",$line_json['date_created']['$date']));
    $overlord_user=$line_json['overlord_user']['$oid'];
    $user_id=$line_json['user_id']['$oid'];
    $country=$line_json['country'];
    $max=json_encode($line_json['max']);
    $min=json_encode($line_json['min']);

    #以下四个正式环境中存在
    $ng=json_encode($line_json['NG']);
    $business_id=$line_json['business_id']['$oid'];
    $created=str_replace("Z","",str_replace("T"," ",$line_json['created']['$date']));
    $merchant_id=$line_json['merchant_id'];

    $sql= "INSERT INTO fees (`id`, `instrument_type`, `service_type`, `config_type`, `service_id`, `date_created`, `overlord_user`, `user_id`, `country`, `max`, `min`, `ng`, `business_id`, `created`, `merchant_id` ) VALUES ('${id}','${instrument_type}','${service_type}','${config_type}','${service_id}','${date_created}','${overlord_user}','${user_id}','${country}','${max}','${min}','${ng}','${business_id}','${created}','${merchant_id}');";
    //echo $sql."\n";
    @error_log($sql."\n",3,$out_filename);

    ####fees_charge 表
    $charges=$line_json['charges'];
    if(!empty($charges) && is_array($charges)) {
        foreach ($charges as $k => $v) {
            $fees_id=$id;
            $max_amount=json_encode($v['max_amount']);
            $min_amount=json_encode($v['min_amount']);
            $absolute_charge=json_encode($v['absolute_charge']);
            $relative_charge=$v['relative_charge'];
            $type=$v['type'];
            $fee_type=$v['fee_type'];

            $sql= "INSERT INTO fees_charge (`fees_id`, `max_amount`, `min_amount`, `absolute_charge`, `relative_charge`, `type`, `fee_type` ) VALUES ('${fees_id}','${max_amount}','${min_amount}','${absolute_charge}','${relative_charge}','${type}','${fee_type}');";
            //echo $sql."\n";
            @error_log($sql."\n",3,$out_filename);
        }
    }
}
?>
