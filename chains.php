#!/usr/bin/php
<?php
define('DS', DIRECTORY_SEPARATOR);
define('ROOT_PATH', realpath(dirname(__FILE__).DS).DS);
include_once (ROOT_PATH."global.php");

$file = @file($in_filename);
foreach($file as $line)
{
    //echo $line;
    $line_json = json_decode($line,true);
    if(!is_array($line_json)){
        @error_log($line."\n",3,"/tmp/mongodb_mysql_error.log");
        continue;
    }
    //var_dump($line_json);

    $id=$line_json['_id']['$oid'];
    $version=$line_json['version'];
    $deleted=$line_json['deleted']?"1":"0";
    $lock_by=$line_json['lock']['by']['$oid'];
    $lock_until=str_replace("Z","",str_replace("T"," ",$line_json['lock']['until']['$date']));
    $updated_at=str_replace("Z","",str_replace("T"," ",$line_json['updated_at']['$date']));

    $box_ids=$line_json['box_ids'];
    if(!empty($box_ids) && is_array($box_ids)){
        foreach ($box_ids as $k=>$v) {
            $box_id = $v['$oid'];
            if(!empty($box_id)) {
                $sql_start="INSERT";
                if($sql_type=="REPLACE"){
                    $sql_start="REPLACE";
                }
                $sql = "$sql_start INTO chains (`id`, `version`, `deleted`, `box_id`, `lock_by`, `lock_until`, `updated_at` ) VALUES ('${id}','${version}','${deleted}','${box_id}','${lock_by}','${lock_until}','${updated_at}');";
                //echo $sql."\n";
                @error_log($sql . "\n", 3, $out_filename);
            }
        }
    }
}
?>
