#!/usr/bin/php
<?php
define('DS', DIRECTORY_SEPARATOR);
define('ROOT_PATH', realpath(dirname(__FILE__).DS).DS);
include_once (ROOT_PATH."global.php");

$file = @file($in_filename);
foreach($file as $line)
{
    //echo $line;
    $line_json = json_decode($line,true);
    if(!is_array($line_json)){
        @error_log($line."\n",3,"/tmp/mongodb_mysql_error.log");
        continue;
    }
    //var_dump($line_json);
    $id=$line_json['_id']['$oid'];
    $name=$line_json['name'];
    $email=$line_json['email'];
    $referral_code=$line_json['referral_code'];
    $password_hash=$line_json['password']['hash']['$binary'];
    $password_salt=$line_json['password']['salt']['$binary'];
    $user_invites=json_encode($line_json['user_invites']);
    $created_at=str_replace("Z","",str_replace("T"," ",$line_json['created_at']['$date']));
    $last_visit=str_replace("Z","",str_replace("T"," ",$line_json['last_visit']['$date']));
    $role=$line_json['role'];

    $sql="INSERT INTO overlord_users (`id`, `name`, `email`, `referral_code`, `password_hash`, `password_salt`, `user_invites`, `created_at`, `last_visit`, `role` ) VALUES ('${id}','${name}','${email}','${referral_code}','${password_hash}','${password_salt}','${user_invites}','${created_at}','${last_visit}','${role}');";
    //echo $sql."\n";
    @error_log($sql."\n",3,$out_filename);
}
?>
