#!/bin/sh
#通过graplql接口查询余额

export DATA_SHELL_PATH=$(cd "$(dirname "$0")"; pwd)

usage() {
    echo "Usage: sh $0 type userid"
    echo "Example: sh $0 [user|businesses|owealth] ID"
    echo "sh $0 user 5d7108e3cac6da0001b15010"
    echo "sh $0 businesses 123456123456123456999910"
    echo "sh $0 owealth 5d7108e3cac6da0001b15010"
    exit 1
}

if [ $# != 2 ]; then
    usage
fi

TYPE=$1
ID=$2

#graphql_url="http://admin.nip-dev.opay-test.net/admin/graphql"
graphql_url="https://admin.opay.team/admin/graphql"
#graphql_url="https://operapay.com/graphql"


secret="40830cfb6b025b64854c73b8f491fe83"
cookie="cookie: mellon-cookie=079e5fb3b7dd8cbd3e670701461a8a72; _gorilla_csrf=MTU2NzQzMzI1MnxJbHBxYURSbGVtb3phemRzUkVsSlZsTTNLekJSVFRaRFJUZEVSR1pvUjBjelJYSTFjelpKWlhkV1RuYzlJZ289fEw48xrh_92NHUJiO5FKThnV7kGp8WasYZaByHf4E8mR; sessionid=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9"

ql=""
if [ "${TYPE}" == "owealth" ];then
    ql='{"query":"query{ \n   interestBalance(userID:\"'${ID}'\",secret:\"'${secret}'\") {\n    balance {\n      currency\n      value\n    }\n  }\n}"}'
    jq_path=".data.interestBalance.balance.value"
elif [ "${TYPE}" == "businesses" ];then
    ql='{"query":"query{ \n   businessBalance(id:\"'${ID}'\",countryCode:\"NG\",secret:\"'${secret}'\") {\n    balance {\n      currency\n      value\n    }\n  }\n}"}'
    jq_path=".data.businessBalance.balance.value"
elif [ "${TYPE}" == "user" ];then
    ql='{"query":"query{ \n   userBalance(id:\"'${ID}'\",secret:\"'${secret}'\") {\n    balance {\n      currency\n      value\n    }\n  }\n}"}'
    jq_path=".data.userBalance.balance.value"
fi

if [ "${ql}" == "" ];then
    echo "ql is null"
    exit
fi

balance=`curl -Ss -X POST ${graphql_url} -H 'Accept: application/json'  -H 'Content-Type: application/json' -H "${cookie}" -d "${ql}"|jq -r -c "${jq_path}"`

echo ${balance}