#!/bin/sh
#检查导入行数是否一致
#对比mongo json行数与mysql行数，主要针对整表导入的情况

export DATA_SHELL_PATH=$(cd "$(dirname "$0")"; pwd)
source ${DATA_SHELL_PATH}/config/Config.sh

table="
blacklisted_instrument
business_users
businesses
configuration
fees
financial_institutions
merchant
overlord_users
payment_terminal_providers
terminal_owners
terminals
user
payment_instruments
coins_transactions
chains
chain_boxes
"

check_line() {
    #echo $1
    file_name=${1}
    if [ "$1" == "user" ];then
        file_name="users"
    fi
    if [ "$1" == "merchant" ];then
        file_name="merchants"
    fi

    if [ -f "${DATA_SHELL_PATH}/data/${file_name}.json" ];then
        file_num=`wc -l ${DATA_SHELL_PATH}/data/${file_name}.json |awk -F' ' '{print $1}'`
        mysql_nums=`${BIN_MYSQL} --skip-column-names -e "SELECT COUNT(*) FROM ${DB_NAME}.${1}" 2>/dev/null`

        echo -e "文件:\t${file_num}\t\t\t\t\t库:\t${mysql_nums}\t${1}"
    fi
}


for t in ${table};do
    check_line $t
done

