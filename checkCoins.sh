#!/bin/sh
#检查用户余额正确性支持V2/V3查询
#比对Mongdb与Mysql

export DATA_SHELL_PATH=$(cd "$(dirname "$0")"; pwd)
source ${DATA_SHELL_PATH}/config/Config.sh

usage() {
    echo "Usage: sh $0 type field phonenumber"
    echo "Example: sh $0 [user|businesses] [id|phonenumber] +2348051584655"
    echo "sh $0 user phonenumber +2348051584655"
    echo "sh $0 businesses phonenumber +2348067779014"
    exit 1
}

if [ $# != 3 ]; then
    usage
fi

FIELD_VALUE=$3
FIELD=$2
TYPE=$1

sh ${DATA_SHELL_PATH}/checkCoinsFromMongoDb.sh ${TYPE} ${FIELD} ${FIELD_VALUE}
sh ${DATA_SHELL_PATH}/checkCoinsFromMysql.sh ${TYPE} ${FIELD} ${FIELD_VALUE}