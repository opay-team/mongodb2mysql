<?php
ini_set('display_errors','Off');
error_reporting(E_ERROR);

$in_filename = strtolower(trim($argv[1]));
$out_filename = strtolower(trim($argv[2]));
$sql_type = trim($argv[3]);

if (!file_exists($in_filename) && empty($out_filename))
{
    echo "请输入需要处理的文件名  输出文件名\n";
    echo "例：".$argv[0]." xxxx.json xxx.sql\n";
    exit;
}
?>