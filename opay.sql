-- MySQL dump 10.13  Distrib 5.7.26, for osx10.14 (x86_64)
--
-- Host: 10.4.250.10    Database: opay
-- ------------------------------------------------------
-- Server version	5.6.45-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `badactor_infractions`
--

DROP TABLE IF EXISTS `badactor_infractions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `badactor_infractions` (
  `pk_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(64) NOT NULL,
  `actor` varchar(64) DEFAULT NULL COMMENT '登陆账号',
  `name` varchar(64) DEFAULT NULL,
  `message` varchar(100) DEFAULT NULL COMMENT '描述',
  `strike_limit` varchar(64) DEFAULT NULL COMMENT '限制，如登陆连续登陆失败次数',
  `expire_base` varchar(64) DEFAULT NULL COMMENT '过期时长',
  `sentence` varchar(64) DEFAULT NULL,
  `expire_by` datetime DEFAULT NULL COMMENT '过期时间',
  PRIMARY KEY (`pk_id`) USING BTREE,
  KEY `user_id` (`user_id`),
  KEY `actor` (`actor`),
  KEY `expire_by` (`expire_by`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='违规记录表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `blacklisted_instrument`
--

DROP TABLE IF EXISTS `blacklisted_instrument`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blacklisted_instrument` (
  `id` varchar(64) NOT NULL,
  `pk_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `signature` varchar(255) NOT NULL,
  `number` varchar(64) DEFAULT NULL,
  `scheme` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`pk_id`) USING BTREE,
  UNIQUE KEY `unique_id` (`id`),
  KEY `number` (`number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='支付设备黑名单表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `business_aggregators`
--

DROP TABLE IF EXISTS `business_aggregators`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `business_aggregators` (
  `pk_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `business_id` varchar(64) NOT NULL,
  `authority_id` varchar(64) DEFAULT NULL COMMENT '对应businesses表_id',
  `authority_version` tinyint(2) DEFAULT NULL,
  `authority_type` varchar(64) DEFAULT NULL,
  `status` varchar(64) DEFAULT NULL,
  `tier` varchar(64) DEFAULT NULL,
  `commission` text,
  `created_at` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`pk_id`) USING BTREE,
  KEY `authority_id` (`authority_id`),
  KEY `user_id` (`business_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='用户信息子表aggregators';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `business_users`
--

DROP TABLE IF EXISTS `business_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `business_users` (
  `id` varchar(64) NOT NULL,
  `pk_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `linked_businesses` varchar(255) DEFAULT NULL COMMENT '对应businesses表id字段',
  `email` varchar(255) DEFAULT NULL COMMENT '邮箱',
  `firstname` varchar(64) DEFAULT NULL COMMENT 'First Name',
  `lastname` varchar(64) DEFAULT NULL COMMENT 'Last Name',
  `password_hash` varbinary(255) DEFAULT NULL COMMENT '密码HASH',
  `password_salt` varbinary(255) DEFAULT NULL COMMENT '密码SALT',
  `bvn` varchar(255) DEFAULT NULL COMMENT '关联表_users_bvn',
  `reset_token` varchar(255) DEFAULT NULL COMMENT 'Token',
  `last_visit` datetime DEFAULT NULL COMMENT '最后访问时间',
  `created_at` datetime DEFAULT NULL COMMENT '创建时间',
  `access_role` varchar(64) DEFAULT NULL COMMENT '角色',
  `phone_number` varchar(64) DEFAULT NULL COMMENT '手机号',
  PRIMARY KEY (`pk_id`) USING BTREE,
  UNIQUE KEY `unique_id` (`id`),
  KEY `email` (`email`(191)),
  KEY `last_visit` (`last_visit`),
  KEY `created_at` (`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='商户用户信息表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `businesses`
--

DROP TABLE IF EXISTS `businesses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `businesses` (
  `id` varchar(64) NOT NULL,
  `pk_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `merchant_id` varchar(64) DEFAULT NULL COMMENT '商户ID',
  `linked_users` varchar(255) DEFAULT NULL COMMENT '关联人员',
  `name` varchar(64) DEFAULT NULL COMMENT '名称',
  `category` varchar(64) DEFAULT NULL COMMENT '分类',
  `key_pairs` varchar(64) DEFAULT NULL COMMENT '关联表_商户密钥表',
  `webhook_url` varchar(255) DEFAULT NULL COMMENT 'webhook_url',
  `allowed_to_go_live` tinyint(1) DEFAULT NULL,
  `instant_settlement` tinyint(1) DEFAULT NULL COMMENT '即时结算',
  `countries` varchar(255) DEFAULT NULL COMMENT '国家',
  `skip_commit_stage` tinyint(1) DEFAULT NULL COMMENT '是否跳过提交阶段',
  `pos_transaction_limit` varchar(255) DEFAULT NULL COMMENT 'pos交易限额',
  `user_invites` varchar(64) DEFAULT NULL COMMENT '关联表_用户邀请表',
  `reseller_invites` varchar(64) DEFAULT NULL COMMENT '关联表_商户邀请代理商表',
  `remittance_config` varchar(255) DEFAULT NULL COMMENT '回款配置',
  `email_settings` varchar(64) DEFAULT NULL COMMENT '关联表_商户邮件通知配置表',
  `limits` varchar(255) DEFAULT NULL,
  `disable_settlements` tinyint(1) DEFAULT NULL,
  `timestamp` datetime DEFAULT NULL COMMENT '时间',
  `settlement_period` varchar(64) DEFAULT NULL,
  `phonenumber` varchar(64) DEFAULT NULL COMMENT '手机号',
  `business_type` varchar(64) DEFAULT NULL,
  `sandbox_id` varchar(64) DEFAULT NULL COMMENT 'Business  ID in sandbox DB',
  `active_environment` varchar(255) DEFAULT NULL,
  `fee` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`pk_id`) USING BTREE,
  UNIQUE KEY `unique_id` (`id`),
  UNIQUE KEY `merchant_id` (`merchant_id`),
  KEY `timestamp` (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='商户表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `businesses_merchant_email_settings`
--

DROP TABLE IF EXISTS `businesses_merchant_email_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `businesses_merchant_email_settings` (
  `id` varchar(64) NOT NULL,
  `pk_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `merchant_id` varchar(64) DEFAULT NULL COMMENT '商户ID',
  `enable_transaction_notifications` tinyint(1) DEFAULT NULL COMMENT '是否邮件发送交易通知',
  `enable_transfer_notifications` tinyint(1) DEFAULT NULL COMMENT '是否邮件发送转让通知',
  `enable_low_balance_notifications` tinyint(1) DEFAULT NULL COMMENT '是否邮件发送低余额通知',
  `balance_limit_value` varchar(64) DEFAULT NULL COMMENT '余额限额字符串形式',
  `balance_limit_floatvalue` varchar(64) DEFAULT NULL COMMENT '余额限额字符串形式',
  `balance_limit_currency` varchar(64) DEFAULT NULL COMMENT '余额币种',
  `balance_notification_sent` tinyint(1) DEFAULT NULL COMMENT '是否邮件发送余额通知',
  PRIMARY KEY (`pk_id`) USING BTREE,
  UNIQUE KEY `unique_id` (`id`),
  KEY `merchant_id` (`merchant_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='商户邮件通知配置表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `businesses_merchant_key`
--

DROP TABLE IF EXISTS `businesses_merchant_key`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `businesses_merchant_key` (
  `id` varchar(64) NOT NULL,
  `pk_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `merchant_id` varchar(64) DEFAULT NULL COMMENT '商户ID',
  `public_key` varchar(64) DEFAULT NULL COMMENT '公钥',
  `private_key_cipher_text` varbinary(255) DEFAULT NULL COMMENT '商户私钥内容',
  `private_key_mac` varbinary(255) DEFAULT NULL COMMENT '商户私钥mac值',
  `key_env` varchar(64) DEFAULT NULL COMMENT '商户秘钥所属环境',
  PRIMARY KEY (`pk_id`) USING BTREE,
  UNIQUE KEY `unique_id` (`id`),
  KEY `merchant_id` (`merchant_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='商户密钥表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `businesses_merchant_reseller_invites`
--

DROP TABLE IF EXISTS `businesses_merchant_reseller_invites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `businesses_merchant_reseller_invites` (
  `id` varchar(64) NOT NULL,
  `pk_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `merchant_id` varchar(64) DEFAULT NULL COMMENT '商户ID',
  `email` varchar(255) DEFAULT NULL COMMENT 'EMAIL',
  `token` varchar(255) DEFAULT NULL COMMENT 'TOKEN',
  `created_at` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`pk_id`) USING BTREE,
  UNIQUE KEY `unique_id` (`id`),
  KEY `merchant_id` (`merchant_id`),
  KEY `email` (`email`(191)),
  KEY `created_at` (`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='商户邀请代理商表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `businesses_merchant_user_invites`
--

DROP TABLE IF EXISTS `businesses_merchant_user_invites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `businesses_merchant_user_invites` (
  `id` varchar(64) NOT NULL,
  `pk_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `merchant_id` varchar(64) DEFAULT NULL COMMENT '商户ID',
  `token` varchar(255) DEFAULT NULL COMMENT 'TOKEN',
  `email` varchar(255) DEFAULT NULL COMMENT 'EMAIL',
  `created_at` datetime DEFAULT NULL COMMENT '创建时间',
  `expired` tinyint(1) DEFAULT NULL COMMENT 'expired',
  `extra` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`pk_id`) USING BTREE,
  KEY `merchant_id` (`merchant_id`),
  KEY `email` (`email`(191)),
  KEY `created_at` (`created_at`),
  KEY `unique_id` (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='用户邀请表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `chain_boxes`
--

DROP TABLE IF EXISTS `chain_boxes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chain_boxes` (
  `pk_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id` varchar(64) DEFAULT NULL,
  `version` varchar(64) DEFAULT NULL,
  `signature` varchar(255) DEFAULT NULL,
  `transaction_id` varchar(64) DEFAULT NULL,
  `transaction_version` int(11) DEFAULT NULL,
  `transaction_incoming` tinyint(1) DEFAULT NULL,
  `transaction_opcode` int(11) DEFAULT NULL,
  `transaction_type` varchar(64) DEFAULT NULL,
  `transaction_account` varchar(64) DEFAULT NULL,
  `transaction_timestamp` datetime DEFAULT NULL,
  `transaction_description` varchar(64) DEFAULT NULL,
  `transaction_ext_ref` varchar(64) DEFAULT NULL,
  `transaction_phone_number` varchar(64) DEFAULT NULL,
  `transaction_currency_amount_value` varchar(64) DEFAULT NULL,
  `transaction_currency_amount_floatValue` varchar(64) DEFAULT NULL,
  `transaction_currency_amount_currency` varchar(64) DEFAULT NULL,
  `transaction_string` varchar(64) DEFAULT NULL,
  `transaction_meta` varchar(64) DEFAULT NULL,
  `transaction_signature` varchar(255) DEFAULT NULL,
  `transaction_currency_amount` varchar(512) DEFAULT NULL,
  `transaction_balance_after` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`pk_id`),
  KEY `id` (`id`) USING BTREE,
  KEY `transaction_timestamp` (`transaction_timestamp`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `chains`
--

DROP TABLE IF EXISTS `chains`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chains` (
  `pk_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id` varchar(64) DEFAULT NULL,
  `version` varchar(64) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `box_id` varchar(64) DEFAULT NULL,
  `lock_by` varchar(255) DEFAULT NULL,
  `lock_until` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`pk_id`),
  UNIQUE KEY `id_box_id` (`id`,`box_id`) USING BTREE,
  KEY `updated_at` (`updated_at`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `coins_transactions`
--

DROP TABLE IF EXISTS `coins_transactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `coins_transactions` (
  `id` varchar(64) NOT NULL,
  `pk_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `previous_id` varchar(64) DEFAULT NULL,
  `chain_id` varchar(64) DEFAULT NULL,
  `counter` bigint(20) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `type` varchar(64) DEFAULT NULL,
  `timestamp` datetime DEFAULT NULL,
  `reference` varchar(64) DEFAULT NULL,
  `amount` varchar(255) DEFAULT NULL,
  `balance_before` varchar(512) DEFAULT NULL,
  `balance_after` varchar(512) DEFAULT NULL,
  `signature` varchar(255) DEFAULT NULL,
  `data` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`pk_id`) USING BTREE,
  UNIQUE KEY `unique_id` (`id`),
  UNIQUE KEY `chain_id_counter` (`chain_id`,`counter`),
  KEY `timestamp` (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `configuration`
--

DROP TABLE IF EXISTS `configuration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `configuration` (
  `id` varchar(64) NOT NULL,
  `pk_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `name` varchar(64) DEFAULT NULL,
  `flags` text,
  PRIMARY KEY (`pk_id`) USING BTREE,
  UNIQUE KEY `unique_id` (`id`),
  KEY `id` (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `document`
--

DROP TABLE IF EXISTS `document`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `document` (
  `id` varchar(64) NOT NULL,
  `pk_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `user_id` varchar(64) DEFAULT NULL,
  `type` varchar(64) DEFAULT NULL,
  `file` varchar(255) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `upload_date` datetime DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `reject_reason` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`pk_id`) USING BTREE,
  KEY `id` (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fees`
--

DROP TABLE IF EXISTS `fees`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fees` (
  `id` varchar(64) NOT NULL COMMENT 'ID',
  `pk_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `instrument_type` int(2) DEFAULT NULL COMMENT '支付方式:枚举值',
  `service_type` varchar(16) DEFAULT NULL COMMENT '业务类型',
  `config_type` varchar(16) DEFAULT NULL COMMENT '配置类型:main',
  `service_id` varchar(32) DEFAULT NULL COMMENT '业务编号',
  `date_created` datetime DEFAULT NULL COMMENT '创建时间',
  `overlord_user` varchar(64) DEFAULT NULL COMMENT 'admin用户ID',
  `user_id` varchar(64) DEFAULT NULL COMMENT '用户ID',
  `country` varchar(64) DEFAULT NULL COMMENT '国家编码:NGA',
  `max` varchar(64) DEFAULT NULL COMMENT '最大值',
  `min` varchar(64) DEFAULT NULL COMMENT '最小值符点',
  `ng` varchar(255) DEFAULT NULL,
  `business_id` varchar(255) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `merchant_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`pk_id`) USING BTREE,
  UNIQUE KEY `unique_id` (`id`),
  KEY `idx_user_id` (`user_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='费用配置表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fees_charge`
--

DROP TABLE IF EXISTS `fees_charge`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fees_charge` (
  `pk_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `fees_id` varchar(64) DEFAULT NULL COMMENT 'feesid',
  `max_amount` varchar(255) DEFAULT NULL COMMENT '最大值',
  `min_amount` varchar(255) DEFAULT NULL COMMENT '最小值',
  `absolute_charge` varchar(255) DEFAULT NULL COMMENT '绝对值',
  `relative_charge` varchar(64) DEFAULT NULL COMMENT '相对值百分比',
  `type` varchar(64) DEFAULT NULL COMMENT 'relative或absolute',
  `fee_type` varchar(64) DEFAULT NULL COMMENT 'charge或deduct',
  PRIMARY KEY (`pk_id`) USING BTREE,
  KEY `idx_fees_id` (`fees_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='费用详细信息表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `financial_institutions`
--

DROP TABLE IF EXISTS `financial_institutions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `financial_institutions` (
  `id` varchar(64) NOT NULL,
  `pk_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `alt_code` varchar(255) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `display_name` varchar(255) DEFAULT NULL,
  `category_code` varchar(255) DEFAULT NULL,
  `category` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `update_batch_number` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`pk_id`) USING BTREE,
  UNIQUE KEY `unique_id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `merchant`
--

DROP TABLE IF EXISTS `merchant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `merchant` (
  `id` varchar(64) NOT NULL,
  `pk_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `name` varchar(128) DEFAULT NULL,
  `fee` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`pk_id`) USING BTREE,
  UNIQUE KEY `unique_id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `overlord_users`
--

DROP TABLE IF EXISTS `overlord_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `overlord_users` (
  `id` varchar(64) NOT NULL,
  `pk_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `name` varchar(64) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `referral_code` varchar(255) DEFAULT NULL,
  `password_hash` varchar(255) DEFAULT NULL,
  `password_salt` varchar(255) DEFAULT NULL,
  `user_invites` text,
  `created_at` datetime DEFAULT NULL,
  `last_visit` datetime DEFAULT NULL,
  `role` varchar(255) DEFAULT NULL COMMENT 'OverlordRole',
  PRIMARY KEY (`pk_id`) USING BTREE,
  UNIQUE KEY `unique_id` (`id`),
  KEY `created_at` (`created_at`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `payment_instruments`
--

DROP TABLE IF EXISTS `payment_instruments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payment_instruments` (
  `id` varchar(64) NOT NULL,
  `pk_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `user_id` varchar(64) NOT NULL,
  `payment_id` varchar(64) DEFAULT NULL,
  `number` varchar(64) DEFAULT NULL,
  `scheme` varchar(64) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `account_number` varchar(64) DEFAULT NULL,
  `signature` varchar(255) DEFAULT NULL,
  `signature_version` int(11) DEFAULT NULL,
  `data_cipher_text` text,
  `data_mac` varchar(255) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`pk_id`) USING BTREE,
  UNIQUE KEY `unique_id` (`id`),
  KEY `user_id` (`user_id`),
  KEY `payment_id` (`payment_id`),
  KEY `created` (`created`),
  KEY `updated` (`updated`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='帐号余额表coins_transactions关联表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `payment_terminal_providers`
--

DROP TABLE IF EXISTS `payment_terminal_providers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payment_terminal_providers` (
  `id` varchar(64) NOT NULL,
  `pk_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `name` varchar(64) DEFAULT NULL,
  `authorization_hash_hash` varbinary(255) DEFAULT NULL,
  `authorization_hash_salt` varbinary(255) DEFAULT NULL,
  `country` varchar(64) DEFAULT NULL,
  `created` datetime DEFAULT NULL COMMENT '创建时间',
  `authorization` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`pk_id`) USING BTREE,
  UNIQUE KEY `unique_id` (`id`),
  KEY `created` (`created`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COMMENT='PaymentTerminalProvider';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `terminal_owners`
--

DROP TABLE IF EXISTS `terminal_owners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `terminal_owners` (
  `id` varchar(64) NOT NULL,
  `pk_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `terminal_id` varchar(64) DEFAULT NULL COMMENT '终端ID',
  `pos_id` varchar(64) DEFAULT NULL COMMENT 'POS机ID',
  `serial` varchar(64) DEFAULT NULL COMMENT 'serial',
  `owner_id` varchar(64) DEFAULT NULL COMMENT '拥有者ID',
  `owner_name` varchar(64) DEFAULT NULL COMMENT '拥有者名称',
  `owner_type` varchar(64) DEFAULT NULL COMMENT '拥有者类型',
  `created` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`pk_id`) USING BTREE,
  UNIQUE KEY `unique_id` (`id`),
  KEY `owner_id` (`owner_id`),
  KEY `created` (`created`),
  KEY `pos_id_terminal_id_serial_owner_id` (`pos_id`,`terminal_id`,`serial`,`owner_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COMMENT='POS机终端用户信息表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `terminals`
--

DROP TABLE IF EXISTS `terminals`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `terminals` (
  `id` varchar(64) NOT NULL,
  `pk_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `pos_id` varchar(64) DEFAULT NULL COMMENT 'POS机ID',
  `payment_terminal_provider_id` varchar(64) DEFAULT NULL COMMENT '支付终端提供者ID',
  `terminal_id` varchar(64) DEFAULT NULL COMMENT '终端ID',
  `created` datetime DEFAULT NULL COMMENT '创建时间',
  `bank` varchar(64) DEFAULT NULL COMMENT '银行',
  `fee` varchar(64) DEFAULT NULL COMMENT 'fee',
  PRIMARY KEY (`pk_id`) USING BTREE,
  UNIQUE KEY `unique_id` (`id`),
  KEY `created` (`created`),
  KEY `pptb` (`payment_terminal_provider_id`,`pos_id`,`terminal_id`,`bank`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='POS机终端信息表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `transactions_owealth`
--

DROP TABLE IF EXISTS `transactions_owealth`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transactions_owealth` (
  `pk_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id` varchar(64) DEFAULT NULL,
  `state_type` varchar(64) DEFAULT NULL,
  `timestamp` datetime DEFAULT NULL,
  `country` varchar(64) DEFAULT NULL,
  `service_type` varchar(64) DEFAULT NULL,
  `service_id` varchar(64) DEFAULT NULL,
  `value` varchar(64) DEFAULT NULL,
  `floatValue` varchar(64) DEFAULT NULL,
  `currency` varchar(64) DEFAULT NULL,
  `recipient_type` varchar(255) DEFAULT NULL,
  `recipient_user_id` varchar(64) DEFAULT NULL,
  `recipient_user_phonenumber` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`pk_id`),
  UNIQUE KEY `id` (`id`),
  KEY `timestamp` (`timestamp`),
  KEY `recipient_user_id` (`recipient_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `transactions_owealth_balance`
--

DROP TABLE IF EXISTS `transactions_owealth_balance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transactions_owealth_balance` (
  `pk_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(64) DEFAULT NULL,
  `phonenumber` varchar(64) DEFAULT NULL,
  `invest_amount` varchar(64) DEFAULT '0' COMMENT '投资',
  `invest_interest_amount` varchar(64) DEFAULT '0' COMMENT '收益',
  `invest_deduct_amount` varchar(64) DEFAULT '0' COMMENT '扣款',
  `coins_transfer_amount` varchar(64) DEFAULT '0' COMMENT '提现',
  `balance` varchar(64) DEFAULT '0' COMMENT '余额_通过加减获得',
  `balance_floatvalue` decimal(20,2) unsigned DEFAULT 0.00 COMMENT '余额_通过balance_after获得_最准确',
  `balance_floatvalue_2` decimal(20,2) unsigned DEFAULT 0.00 COMMENT '余额_通过v2累加减获得',
  PRIMARY KEY (`pk_id`),
  UNIQUE KEY `user_id` (`user_id`) USING BTREE,
  UNIQUE KEY `phonenumber` (`phonenumber`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=10017 DEFAULT CHARSET=utf8mb4 COMMENT='Owealth';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` varchar(64) NOT NULL,
  `pk_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `name` varchar(128) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `last_visit` datetime DEFAULT NULL,
  `phonenumber` varchar(64) DEFAULT NULL,
  `email_verified` tinyint(1) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `hash` varchar(255) DEFAULT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `kyc_level` varchar(255) DEFAULT NULL,
  `upgraded_date` datetime DEFAULT NULL,
  `role` varchar(64) DEFAULT NULL,
  `referral_code` varchar(64) DEFAULT NULL,
  `referrer_code` varchar(64) DEFAULT NULL,
  `limit` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`pk_id`) USING BTREE,
  UNIQUE KEY `unique_id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_aggregators`
--

DROP TABLE IF EXISTS `user_aggregators`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_aggregators` (
  `pk_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `user_id` varchar(64) NOT NULL,
  `update_by` varchar(64) DEFAULT NULL,
  `authority_id` varchar(64) DEFAULT NULL COMMENT '对应businesses表_id',
  `authority_version` tinyint(2) DEFAULT NULL,
  `authority_type` varchar(64) DEFAULT NULL,
  `status` varchar(64) DEFAULT NULL,
  `tier` varchar(64) DEFAULT NULL,
  `commission` text,
  `created_at` datetime DEFAULT NULL COMMENT '创建时间',
  `aggregators_type` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`pk_id`) USING BTREE,
  KEY `authority_id` (`authority_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='用户信息子表aggregators';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_bvn`
--

DROP TABLE IF EXISTS `user_bvn`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_bvn` (
  `id` varchar(64) NOT NULL COMMENT '用户ID',
  `pk_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `number` varchar(64) DEFAULT NULL,
  `provider` varchar(64) DEFAULT NULL,
  `transactionid` varchar(64) DEFAULT NULL,
  `first_name` varchar(64) DEFAULT NULL,
  `middle_name` varchar(64) DEFAULT NULL,
  `last_name` varchar(64) DEFAULT NULL,
  `full_name` varchar(64) DEFAULT NULL,
  `phonenumber` varchar(64) DEFAULT NULL,
  `dob` varchar(64) DEFAULT NULL COMMENT '生日',
  `gender` varchar(64) DEFAULT NULL,
  `address` varchar(64) DEFAULT NULL,
  `image_id` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`pk_id`) USING BTREE,
  UNIQUE KEY `unique_id` (`id`),
  KEY `phonenumber` (`phonenumber`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='用户信息子表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_detail`
--

DROP TABLE IF EXISTS `user_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_detail` (
  `id` varchar(64) NOT NULL COMMENT '用户ID',
  `pk_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `first_name` varchar(128) DEFAULT NULL,
  `middle_name` varchar(128) DEFAULT NULL,
  `business_name` varchar(128) DEFAULT NULL,
  `surname` varchar(128) DEFAULT NULL,
  `dob` varchar(255) DEFAULT NULL,
  `email_to_verify` varchar(255) DEFAULT NULL,
  `email_verification_id` varchar(64) DEFAULT NULL,
  `email_verification_date` datetime DEFAULT NULL,
  `email_verification_cooldown` datetime DEFAULT NULL,
  `email_verification_last_sent` datetime DEFAULT NULL,
  `verification_emails_sent` int(3) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `verified` tinyint(1) DEFAULT NULL,
  `verified_by_id` varchar(64) DEFAULT NULL,
  `verification_date` datetime DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `country` varchar(64) DEFAULT NULL,
  `city` varchar(64) DEFAULT NULL,
  `lga` varchar(255) DEFAULT NULL,
  `migrated_aggregator` tinyint(1) DEFAULT NULL,
  `limits` varchar(255) DEFAULT NULL,
  `terms_and_conditions_config` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`pk_id`) USING BTREE,
  UNIQUE KEY `unique_id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_identification`
--

DROP TABLE IF EXISTS `user_identification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_identification` (
  `id` varchar(64) NOT NULL,
  `pk_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `type` varchar(255) DEFAULT NULL,
  `number` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`pk_id`) USING BTREE,
  UNIQUE KEY `unique_id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_linked_users`
--

DROP TABLE IF EXISTS `user_linked_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_linked_users` (
  `pk_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(64) NOT NULL,
  `linked_user_id` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`pk_id`) USING BTREE,
  KEY `user_id` (`user_id`),
  KEY `linked_user_id` (`linked_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='用户信息子表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_reseller_invite`
--

DROP TABLE IF EXISTS `user_reseller_invite`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_reseller_invite` (
  `id` varchar(64) NOT NULL,
  `pk_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `user_id` varchar(64) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`pk_id`) USING BTREE,
  UNIQUE KEY `unique_id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_service_config`
--

DROP TABLE IF EXISTS `user_service_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_service_config` (
  `id` varchar(64) NOT NULL,
  `pk_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `user_id` varchar(64) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `enable` tinyint(1) DEFAULT NULL,
  `config` varchar(500) DEFAULT NULL,
  `service_id` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`pk_id`) USING BTREE,
  KEY `user_id` (`user_id`),
  KEY `id` (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_telesale`
--

DROP TABLE IF EXISTS `user_telesale`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_telesale` (
  `id` varchar(64) NOT NULL COMMENT '用户ID',
  `number` varchar(128) DEFAULT NULL,
  `team` varchar(128) DEFAULT NULL,
  `pk_id` bigint(20) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`pk_id`) USING BTREE,
  UNIQUE KEY `id` (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_upgrade`
--

DROP TABLE IF EXISTS `user_upgrade`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_upgrade` (
  `id` varchar(64) NOT NULL,
  `pk_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(64) NOT NULL,
  `role` varchar(64) DEFAULT NULL,
  `accept_by` varchar(64) DEFAULT NULL,
  `document_confirmed_by` varchar(64) DEFAULT NULL,
  `document_confirmed_date` datetime DEFAULT NULL,
  `upgrade_status` varchar(64) DEFAULT NULL,
  `doc_status` varchar(64) DEFAULT NULL,
  `upgrade_submit_date` datetime DEFAULT NULL,
  PRIMARY KEY (`pk_id`) USING BTREE,
  UNIQUE KEY `id` (`id`) USING BTREE,
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='用户升级信息表';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-09-06 13:26:56
