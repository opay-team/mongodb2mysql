#!/bin/sh

export DATA_SHELL_PATH=$(cd "$(dirname "$0")"; pwd)
source ${DATA_SHELL_PATH}/config/Config.sh
source ${DATA_SHELL_PATH}/config/Util.sh

#临时文件配置
#导出表名
_TABLE_NAME="transactions"
#日志处理脚本名称
_PHP_SHELL_NAME="transactionsForOwealth.php"
#日志分割行数
_LOG_SPLIT_NUM="5000"
#MONGODB导出的JSON文件
_TMP_DATA_JSON=${DATA_SHELL_PATH}/data/${_TABLE_NAME}.json

date
echo ">>>开始处理数据:${_TABLE_NAME}"

#删除数据
${BIN_MYSQL} -e "TRUNCATE TABLE ${DB_NAME}.transactions_owealth" 2>/dev/null

#从MongoDB中导出表数据Json格式-7.2 owealth开始有数据
${BIN_MONGODB_EXPORT} -d ${MONGODB_NAME} -c ${_TABLE_NAME} --query "{ 'state.type': 'Complete', 'timestamp': { \$gte: ISODate('2019-06-01T00:00:00.000Z') }, \$or: [ { 'payment_data.service_type': 'invest' }, { 'payment_data.service_type': 'invest-interest' }, { 'payment_data.service_type': 'invest-deduct' }, { \$and: [ { 'payment_data.service_type': 'coins-transfer' }, { 'instrument.scheme': 'Investment' } ] } ] }" -o ${_TMP_DATA_JSON}

echo ">>>导出MongoDB数据结束:"
date
echo ">>>MongoDB->Mysql:"

#批量处理日志存放目录
_TMP_DATA_SUBDIR=${DATA_SHELL_PATH}/data/${_TABLE_NAME}/

mkdir -p ${_TMP_DATA_SUBDIR}
mkdir -p ${_TMP_DATA_SUBDIR}/tmp/
mkdir -p ${_TMP_DATA_SUBDIR}/txt/
mkdir -p ${_TMP_DATA_SUBDIR}/sql/


rm -rf ${_TMP_DATA_SUBDIR}/split_*
rm -rf ${_TMP_DATA_SUBDIR}/tmp/split_*
rm -rf ${_TMP_DATA_SUBDIR}/txt/split_*
rm -rf ${_TMP_DATA_SUBDIR}/sql/split_*


#分割原始文件
split -l ${_LOG_SPLIT_NUM} ${_TMP_DATA_JSON} "${_TMP_DATA_SUBDIR}/split_${_TABLE_NAME}."

for f in `find ${_TMP_DATA_SUBDIR}/ -name "split_*"`;do
    f=${f##*/}

#多进程放后台并发执行
{

    sql_file="${_TMP_DATA_SUBDIR}/txt/${f}.txt"

    rm -rf ${sql_file}
    #将MongDB Json格式导成Mysql表格式
    php ${DATA_SHELL_PATH}/${_PHP_SHELL_NAME} "${_TMP_DATA_SUBDIR}/${f}" "${sql_file}"

    echo ">>>导入Mysql:${sql_file}"

    #导入数据
    if [ -f "${sql_file}" ];then
        ${BIN_MYSQL} -f ${DB_NAME} < ${sql_file} 2>>${DATA_SHELL_PATH}/tmp/${_TABLE_NAME}.error.log
    fi
} &

done

wait

import_nums=`${BIN_MYSQL} --skip-column-names -e "SELECT COUNT(*) FROM ${DB_NAME}.transactions_owealth" 2>/dev/null`
echo ">>>导入数据条数：${import_nums}"



#清空数据
${BIN_MYSQL} -e "TRUNCATE TABLE ${DB_NAME}.transactions_owealth_balance" 2>/dev/null
rm -rf "${DATA_SHELL_PATH}/data/transactions_owealth_balance.sql"

${BIN_MYSQL} --skip-column-names -e "SELECT recipient_user_id,recipient_user_phonenumber,service_type,sum(value) as amount FROM ${DB_NAME}.transactions_owealth GROUP BY recipient_user_id,service_type" > "${DATA_SHELL_PATH}/data/transactions_owealth_balance.tmp" 2>>${DATA_SHELL_PATH}/tmp/${_TABLE_NAME}.error.log

php ${DATA_SHELL_PATH}/transactionsForOwealthBalance.php "${DATA_SHELL_PATH}/data/transactions_owealth_balance.tmp" "${DATA_SHELL_PATH}/data/transactions_owealth_balance.sql"

if [ -f "${DATA_SHELL_PATH}/data/transactions_owealth_balance.sql" ];then
    ${BIN_MYSQL} -f ${DB_NAME} < "${DATA_SHELL_PATH}/data/transactions_owealth_balance.sql" 2>/dev/null
    #余额=充值+收益-扣除-提现-收益*10%
    ${BIN_MYSQL} -e "UPDATE ${DB_NAME}.transactions_owealth_balance SET balance=invest_amount+invest_interest_amount-invest_deduct_amount-coins_transfer_amount-invest_interest_amount*0.1" 2>>${DATA_SHELL_PATH}/tmp/${_TABLE_NAME}.error.log
fi

import_nums=`${BIN_MYSQL} --skip-column-names -e "SELECT COUNT(*) FROM ${DB_NAME}.transactions_owealth_balance" 2>/dev/null`
echo ">>>导入Owealth用户条数：${import_nums}"
date

echo ">>>查询Owealth用户帐户余额："
date

${BIN_MYSQL} --skip-column-names -e "SELECT user_id FROM ${DB_NAME}.transactions_owealth_balance" > "${DATA_SHELL_PATH}/data/transactions_owealth_balance_uid.tmp"  2>/dev/null

rm -rf "${DATA_SHELL_PATH}/data/transactions_owealth_balance_uid.sql"



split -l 200 "${DATA_SHELL_PATH}/data/transactions_owealth_balance_uid.tmp" "${_TMP_DATA_SUBDIR}/sql/split_transactions_owealth_balance_uid.tmp."

for ff in `find ${_TMP_DATA_SUBDIR}/sql/ -name "split_*"`;do

#多进程放后台并发执行
{

    while read line;do
        user_id=${line}

        main_data_cipher_text_string=`${BIN_MONGODB_EXPORT} -d ${MONGODB_NAME} -c payment_instruments -f 'data' --quiet --query "{'user_id' : ObjectId('${user_id}'),'scheme':'Investment'}"`
        main_data_cipher_text_string=`echo "${main_data_cipher_text_string}"|sed 's/\"$oid\"/\"oid\"/g'`
        main_data_cipher_text_string=`echo "${main_data_cipher_text_string}"|sed 's/\"$binary\"/\"binary\"/g'`
        main_data_cipher_text=`echo "${main_data_cipher_text_string}" | jq -r -c '.data.cipher_text.binary'`


        #解密
        json_string=`${DATA_SHELL_PATH}/decrypt_production -text="${main_data_cipher_text}"`

        box_name=`echo "${json_string}" | jq -r -c '.box_name'`
        chain_id=`echo "${json_string}" | jq -r -c '.chain_id'`

        if [ "${chain_id}" != "" ] && [ "${chain_id}" != "null" ] ;then
            balance_after=`${BIN_MONGODB_EXPORT} -d ${MONGODB_NAME} -c coins_transactions -f 'balance_after' --quiet --query "{'chain_id' : ObjectId('${chain_id}')}" --sort="{'counter': -1}" --limit=1`
            amount=`echo "${balance_after}" | jq -r -c '.balance_after.available_balance.value'`
            available_balance=`echo "${balance_after}" | jq -c '.balance_after.available_balance'`
            #echo -e "MongoDBV3:\t${user_id}\t${amount}\t${chain_id}\t${available_balance}"
            echo "UPDATE ${DB_NAME}.transactions_owealth_balance set balance_floatvalue='${amount}' where user_id='${user_id}';" >> ${DATA_SHELL_PATH}/data/transactions_owealth_balance_uid.sql
        else
            if [ "${box_name}" != "" ] && [ "${box_name}" != "null" ] ;then
                box_id_string=`${BIN_MONGODB_EXPORT} -d ${MONGODB_NAME} -c chains -f 'box_ids' --quiet --query "{'_id' : ObjectId('${box_name}')}"`
                box_id_string=`echo "${box_id_string}"|sed 's/\"$oid\"/\"oid\"/g'`
                box_id=`echo "${box_id_string}"|jq -c ".box_ids[-1]"|jq -r -c ".oid"`

                transactions_string=`${BIN_MONGODB_EXPORT} -d ${MONGODB_NAME} -c chain_boxes -f 'transactions' --quiet --query "{'_id' : ObjectId('${box_id}')}"`
                transactions_string=`echo "${transactions_string}"|sed 's/\"$oid\"/\"oid\"/g'`
                transactions_string=`echo "${transactions_string}"|jq -c ".transactions[-1]"`
                transaction_balance_after=`echo "${transactions_string}"|jq -r -c ".transaction.balance_after"`

                amount=`echo "${transaction_balance_after}" | jq -r -c '.NGN.investment.floatValue'`
                #transaction_balance_after=`echo "${transaction_balance_after}" | jq -c '.NGN'`
                #echo -e "MongoDBV2:\t${user_id}\t${amount}\t${box_name}\t${box_id}\t${transaction_balance_after}"
                echo "UPDATE ${DB_NAME}.transactions_owealth_balance set balance_floatvalue='${amount}' where user_id='${user_id}';" >> ${DATA_SHELL_PATH}/data/transactions_owealth_balance_uid.sql
            fi
        fi

        #balance_floatvalue_2=`sh ${DATA_SHELL_PATH}/checkCoinsFromMysqlForOwealth.sh id ${user_id}|awk -F' ' '{print $3}'`
        #echo "UPDATE ${DB_NAME}.transactions_owealth_balance set balance_floatvalue_2='${balance_floatvalue_2}' where user_id='${user_id}';" >> ${DATA_SHELL_PATH}/data/transactions_owealth_balance_uid.sql

    done < "${ff}"

} &

done

wait

${BIN_MYSQL} -f ${DB_NAME} < "${DATA_SHELL_PATH}/data/transactions_owealth_balance_uid.sql" 2>>${DATA_SHELL_PATH}/tmp/${_TABLE_NAME}.error.log

echo ">>>处理完毕"
date