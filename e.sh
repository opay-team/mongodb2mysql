#!/bin/sh

export DATA_SHELL_PATH=$(cd "$(dirname "$0")"; pwd)

cd ${DATA_SHELL_PATH}

sh ./fees.sh > tmp/fees.log
sh ./blacklisted_instrument.sh > tmp/blacklisted_instrument.log
sh ./financial_institutions.sh > tmp/financial_institutions.log
sh ./configuration.sh > tmp/configuration.log
sh ./overlord_users.sh > tmp/overlord_users.log
sh ./terminals.sh > tmp/terminals.log
sh ./terminal_owners.sh > tmp/terminal_owners.log
sh ./payment_terminal_providers.sh > tmp/payment_terminal_providers.log
sh ./merchant.sh > tmp/merchant.log
sh ./businesses.sh > tmp/businesses.log
sh ./business_users.sh > tmp/business_users.log

sh ./user.sh > tmp/user.log &

#以下是余额相关的 V3 V2
#sh ./payment_instruments.sh > tmp/payment_instruments.log &
#sh ./coins_transactions.sh > tmp/coins_transactions.log &

#sh ./chains.sh > tmp/chains.log &
#sh ./chain_boxes.sh > tmp/chain_boxes.log &

#OWealth
#sh ./transactionsForOwealth.sh > tmp/transactionsForOwealth.log &

